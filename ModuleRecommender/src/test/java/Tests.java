import com.co.app.modrec.domain.CurriculumNode;
import com.co.app.modrec.domain.CurriculumVersion;
import com.co.app.modrec.domain.statistics.CurriculumVersionModulesAttendancePercentageStatistic;
import com.co.app.modrec.domain.statistics.CurriculumVersionStudentsStatistic;
import com.teklabs.gwt.i18n.server.LocaleProxy;
import org.junit.Test;

import java.util.List;
import java.util.Locale;
import org.junit.Ignore;

/* Test comment */
/**
 * Testing
 *
 * @author Lucas Reeh
 */
public class Tests {

    @Ignore
    @Test
    public void testDBStmts() {
        List<CurriculumVersionStudentsStatistic> list = CurriculumVersion.getStudentStatistics(new Long(343));
        for (CurriculumVersionStudentsStatistic o: list) {
            System.out.println(o.getCurriculumVersionId()+" "+o.getNumberOfStudents()+" "+o.getStudyStatusType());
        }
        //assertTrue(true);
    }

    @Ignore
    @Test
    public void testDBStmts2() {
        List<CurriculumVersionModulesAttendancePercentageStatistic> list = CurriculumVersion.getModuleAttendancePercentageStatistics(new Long(343));
        for (CurriculumVersionModulesAttendancePercentageStatistic o: list) {
            System.out.println(o.getCurriculumVersionId()+" "+o.getLimit()+" "+o.getNumberOfModules());
        }
        //assertTrue(true);
    }

    @Ignore
    @Test
    public void testDBStmts3() {
        List<CurriculumNode> list = CurriculumNode.findAllNonModuleNodesByCurrVersId(new Long(343));
        LocaleProxy.setLocale(Locale.ENGLISH);
        for (CurriculumNode o: list) {
            System.out.println(o.getName());
        }
        //assertTrue(true);
    }


}
