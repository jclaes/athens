import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.junit.runners.Suite.SuiteClasses;

/**
 * All Tests
 *
 * @author Lucas Reeh
 */
@RunWith(Suite.class)
@SuiteClasses({
        Tests.class
})
public class AllTests {
}
