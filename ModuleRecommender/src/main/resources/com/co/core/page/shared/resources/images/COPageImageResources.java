package com.co.core.page.shared.resources.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Class holding image resources
 * 
 * @author Lucas Reeh
 *
 */
public interface COPageImageResources extends ClientBundle {

  @Source("co_custom_header_logo.png")
  ImageResource header_logo();

  @Source("surfboard_16.png")
  ImageResource surf_board_16();
  
  @Source("paper_plane_16.png")
  ImageResource paper_plane_16();
  
}