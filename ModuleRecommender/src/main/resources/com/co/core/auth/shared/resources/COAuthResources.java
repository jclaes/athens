package com.co.core.auth.shared.resources;

import com.co.core.auth.shared.resources.images.COAuthImageResources;
import com.co.core.auth.shared.resources.text.COAuthTextConstants;
import com.google.gwt.core.client.GWT;

/**
 * Class Resource helper
 * 
 * @author Lucas Reeh
 *
 */
public class COAuthResources {

	/**
	 * helper variable for accessing image resources
	 */
	public static final COAuthImageResources IMAGES = GWT.create(COAuthImageResources.class);

	/**
	 * helper variable for accessing text resources
	 */
	public static final COAuthTextConstants TEXT = GWT.create(COAuthTextConstants.class);

  
}
