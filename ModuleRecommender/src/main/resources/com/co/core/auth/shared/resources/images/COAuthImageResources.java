package com.co.core.auth.shared.resources.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * Class holding image resources
 * 
 * @author Lucas Reeh
 *
 */
public interface COAuthImageResources extends ClientBundle {

  @Source("icon_key_normal.gif")
  ImageResource key();
  
  @Source("user.png")
  ImageResource user();
  
  @Source("key_16.png")
  ImageResource key_16();
  
  @Source("lock_16.png")
  ImageResource lock_16();
  
  @Source("icon_keyan_normal.gif")
  ImageResource lock_with_key();


}