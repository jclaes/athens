package com.co.core.auth.shared.resources.text;

import com.google.gwt.i18n.client.Messages;

/**
 * Class holding text resources
 * 
 * @author Lucas Reeh
 *
 */
public interface COAuthTextConstants extends Messages {

	String login();

	String signIn();

	String logout();

	String username();

	String password();
	
	String userInformation();
	
	String cancel();
	
	String accountTypeLabel();
	
	String msgLoginIncorrect();
	
	String msgSucessfullyLoggedIn();
	
	String msgLogoutError();
	
	String msgSucessfullyLoggedOut();
	
	String msgErrorCheckingSessionValidity();
	
	String msgNotLoggedIn();
	
	String msgUserPasswordIncorrect();
	
	String msgErrorInLoginDataForm();
	
	String msgPageNotFound();
	
	String msgPleaseLogIn();
	
	String anonymous();
	
	String msgNoRightOnService();
}
