/**
 *
 */
package com.co.app.modrec.shared.resources.text;

import com.google.gwt.i18n.client.Messages;

/**
 * Class holding text resources
 * 
 * @author Lucas Reeh
 * 
 */
public interface COModuleRecommenderTextConstants extends Messages {

	String moduleRecommenderTitle();

	String myCurriculumVersions();

	String curriculumVersionNameLabel();

	String curriculaVersionLabel();

	String curriculaVersionNoResults();

	String navElementCurrVersionSelection();

	String navElementShowAllModules();

	String navElementShowBestAttended();

	String navElementShowBestForMaster();

	String attendance();

	String attendanceInactive();

	String linkToCO(String customSystemName);

	String recommendedSemster();

	String recommendedSemsterShort();

	String recommendedSemsterLong(String customName);

	String recommendationFromStudents();

	String recommendationFromStudentsLong();

	String recommendedByLecturer();

	String semester();

	String semesterShort();

	String searchModules();

	String search();

	String noCurriculumId();

	String noRecommendationsFound();

	String calculatingRecommendations();

	String editRecommendation();

	String close();

	String save();

	String delete();

	String errorModuleHasNoId();

	String errorUnknown();

	String infoSuccessfull();

	String numberOfStudents();

	String numberOfStudentsUnknown();

	String dragHereInfo();

	String lecturerRecommendationExists();

	String loadingStudentsCountBusyMsg();

	String studentRecommendationText();

	String recommendationDialogTitle();

	String infoBoxContent();

	String attendancePercentage();

	String attendanceInactivePercentage();

	String recommendedByLecturerLong();

	String semesterLong();

	String attendanceInfo();

	String attendanceInactiveInfo();

	String viewRecommendation();

	String addRecommendation();

	String helpTitleText();

	String alreadyAttended();

	String statistics();

	String statisticsNumberOfStudents();

	String statisticsStudyStatus();

	String statisticsAttendance();

	String statisticsStudents();

	String statisticsAttendanceSubjectType();

	String statisticsAttendanceMinNumberOfStudents();

	String statisticsAttendanceAvgNumberOfStudents();

	String statisticsAttendanceMaxNumberOfStudents();

	String statisticsAttendancePercentage();

	String statisticsNumberOfModules();

	String statisticsAttendancePercentageRange();

	String subjectTypeCompulsory();

	String subjectTypeElective();

	String subjectTypeCompulsoryElective();

	String subjectTypeNone();

	String myModules();

	String recommendedSemsterCut();

	String semesterCut();

	String numberOfStudentsOfTypeAllActive();

	String numberOfStudentsOfTypeFirstSemester();

	String numberOfStudentsOfTypeVacation();

	String numberOfStudentsOfTypeAllInactive();

	String numberOfStudentsOfTypeGraduated();

	String numberOfStudentsOfTypeDropouts();
}
