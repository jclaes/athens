package com.co.core.page.client.footer;

import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.co.core.page.client.COPagePresenter;
import com.co.core.page.shared.resources.COPageResources;
import com.co.core.shared.resources.COResources;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Image;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;

/**
 * CO Page Footer presenter
 * 
 * presenter for page footer
 * 
 * @author Lucas Reeh
 * 
 */
public class COPageFooterPresenter extends
		Presenter<COPageFooterPresenter.MyView, COPageFooterPresenter.MyProxy> {

	final AbstractImagePrototype imgSurfBoardPrototype = AbstractImagePrototype
			.create(COPageResources.IMAGES.surf_board_16());
	final Image imageSurfBoard = imgSurfBoardPrototype.createImage();

	final AbstractImagePrototype imgPaperPlanePrototype = AbstractImagePrototype
			.create(COPageResources.IMAGES.paper_plane_16());
	final Image imagePaperPlane = imgPaperPlanePrototype.createImage();

	/**
	 * default view interface
	 * 
	 * @author Lucas Reeh
	 * 
	 */
	public interface MyView extends View {

		/**
		 * @return label holding copyright text
		 */
		public Anchor getAnchorCopyright();

		/**
		 * @return label holding support text
		 */
		public Anchor getAnchorSupport();
	}

	/**
	 * code split proxy
	 * 
	 * @author Lucas Reeh
	 * 
	 */
	@ProxyCodeSplit
	public interface MyProxy extends Proxy<COPageFooterPresenter> {
	}

	/**
	 * placeManager
	 */
	@Inject
	PlaceManager placeManager;

	/**
	 * Class constructor
	 * 
	 * @param eventBus
	 * @param view
	 * @param proxy
	 */
	@Inject
	public COPageFooterPresenter(final EventBus eventBus, final MyView view,
			final MyProxy proxy) {
		super(eventBus, view, proxy);
	}

	/**
	 * reveal in parent
	 */
	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, COPagePresenter.SLOT_footerContent, this);
	}

	/**
	 * bindings, registering of handlers and selection handlers of user
	 * interface elements
	 */
	@Override
	protected void onBind() {
		super.onBind();
	}

	/**
	 * setting initial contents of view elements
	 */
	@Override
	protected void onReset() {
		super.onReset();

		getView().getAnchorSupport().setHref(
				"mailto:"
						+ COResources.TEXT.support_email_adress()
						+ "?Subject="
						+ COModuleRecommenderResources.TEXT
								.moduleRecommenderTitle() + "("
						+ COPageResources.TEXT.customTitle() + ")");
		getView().getAnchorSupport().setHTML(
				imagePaperPlane.toString()
						+ COResources.TEXT
								.support());

		getView().getAnchorCopyright().setHref(
				"mailto:"
						+ "lucas.reeh@tum.de"
						+ "?Subject="
						+ COModuleRecommenderResources.TEXT
								.moduleRecommenderTitle());
		getView().getAnchorCopyright().setHTML(
				imageSurfBoard.toString()
						+ COPageResources.TEXT
								.copyrightText());
	}
}
