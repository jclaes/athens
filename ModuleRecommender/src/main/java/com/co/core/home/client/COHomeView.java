package com.co.core.home.client;

import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.co.core.home.shared.resources.COHomeResources;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer;
import com.sencha.gxt.widget.core.client.container.MarginData;

/**
 * Home view
 * 
 * presents home page an logo
 * 
 * @author Lucas Reeh
 * 
 */
public class COHomeView extends ViewImpl implements COHomePresenter.MyView {

	private final Widget widget;

	public interface Binder extends UiBinder<Widget, COHomeView> {
	}

	@UiField
    Image imgLogoBig;

    @UiField
    Label welcomeLabel;

    @UiField
    HTMLPanel htmlPanel;

    @UiField(provided = true)
    BorderLayoutContainer.BorderLayoutData eastData = new BorderLayoutContainer.BorderLayoutData(300);

    @UiField(provided = true)
    MarginData centerData = new MarginData();
	
	/**
	 * Class constructor
	 * 
	 * @param binder
	 */
	@Inject
	public COHomeView(final Binder binder) {
        // ui design
        eastData.setMargins(new Margins(10, 10, 10, 10));
        eastData.setCollapsible(false);
        eastData.setSplit(false);

		widget = binder.createAndBindUi(this);
        getHtmlPanel().add(new HTML(COModuleRecommenderResources.TEXT.infoBoxContent()));
        getWelcomeLabel().setText(COHomeResources.TEXT.homeTextWelcome());
	}

	/**
	 * @return view as widget
	 */
	public Widget asWidget() {
		return widget;
	}

	/**
	 * @return the imgLogoBig
	 */
	public Image getImgLogoBig() {
		return imgLogoBig;
	}

	/**
	 * @return the welcomeLabel
	 */
	public Label getWelcomeLabel() {
		return welcomeLabel;
	}

    /**
     * @return the htmlPanel
     */
    public HTMLPanel getHtmlPanel() {
        return htmlPanel;
    }
	
}
