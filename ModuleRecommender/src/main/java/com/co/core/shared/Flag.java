/**
 * 
 */
package com.co.core.shared;

/**
 * @author Lucas Reeh
 *
 */
public final class Flag {
	
	public static final String TRUE = "J";
	public static final String FALSE = "N";

}
