package com.co.core.auth.client.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

/**
 * User Logs Out Event class (generated)
 * 
 * if user selects logout
 * 
 * @author Lucas Reeh
 *
 */
public class COClientUserLogsOutEvent extends
        GwtEvent<COClientUserLogsOutEvent.COClientUserLogsOutHandler> {

	public static Type<COClientUserLogsOutHandler> TYPE = new Type<COClientUserLogsOutHandler>();

	public interface COClientUserLogsOutHandler extends EventHandler {
		void onCOClientUserLogsOut(COClientUserLogsOutEvent event);
	}

	public COClientUserLogsOutEvent() {
	}

	@Override
	protected void dispatch(COClientUserLogsOutHandler handler) {
		handler.onCOClientUserLogsOut(this);
	}

	@Override
	public Type<COClientUserLogsOutHandler> getAssociatedType() {
		return TYPE;
	}

	public static Type<COClientUserLogsOutHandler> getType() {
		return TYPE;
	}

	public static void fire(HasHandlers source) {
		source.fireEvent(new COClientUserLogsOutEvent());
	}
}
