/**
 * 
 */
package com.co.core.auth.shared.service;

import com.co.core.auth.server.dao.COClientUser;
import com.co.core.auth.shared.service.proxy.COClientUserProxy;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;

/**
 * Request interface for co client user
 * 
 * @author Lucas Reeh
 *
 */
@Service(COClientUser.class)
public interface COClientUserRequest extends RequestContext {

	Request<COClientUserProxy> getCurrentCOClientUser();
	
}
