/**
 * 
 */
package com.co.core.auth.server.action.validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.co.core.auth.server.helper.CODBHelperSession;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.dispatch.server.actionvalidator.ActionValidator;
import com.gwtplatform.dispatch.shared.Action;
import com.gwtplatform.dispatch.shared.Result;

/**
 * Class for session initialization on Actions
 * 
 * use in ServerModule as parameter
 * 
 * @author Lucas Reeh
 *
 */
public class InitActionValidator implements ActionValidator {

	

	/**
	 * requestProvider
	 */
	private final Provider<HttpServletRequest> requestProvider;

	/**
	 * Class constructor
	 * 
	 * modified to get request provider
	 * 
	 * @param servletContext
	 * @param requestProvider
	 */
	@Inject
	InitActionValidator(final Provider<HttpServletRequest> requestProvider) {
		this.requestProvider = requestProvider;
	}
	
	/**
	 * @return true
	 */
	@Override
	public boolean isValid(Action<? extends Result> action) {
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		HttpSession session = requestProvider.get().getSession();
		Object accountNr = session.getAttribute("login.authenticated");
		if (accountNr != null) {
			coDBHelperSession.initFakeSessionFromWeb((Integer) accountNr, this.requestProvider.get().getLocale());
		}
		return true;
	}
	
	@Override
	protected void finalize() throws Throwable {
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		coDBHelperSession.removeCurrentSession();
		super.finalize();
	}

}
