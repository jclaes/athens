/**
 * 
 */
package com.co.core.auth.client.gatekeeper;

import com.co.core.auth.client.COClientUserHolder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;

/**
 * Gate Keeper for presenter which need user to be authenticated
 * 
 * @author Lucas Reeh
 */
@Singleton
public class LoggedInGatekeeper implements Gatekeeper {

	/**
	 * current user
	 */
	@Inject
	COClientUserHolder coClientUserHolder;
	
	/**
	 * Class constructor
	 * 
	 * @param eventBus
	 */
	@Inject
	public LoggedInGatekeeper() {
	}

	/**
	 * canReveal
	 * 
	 * if user is logged in
	 * 
	 */
	@Override
	public boolean canReveal() {
		boolean loggedIn = false;

		if (coClientUserHolder.getUser() != null) {
			loggedIn = coClientUserHolder.getUser().isLoggedIn();
		}
		return loggedIn;
	}

}
