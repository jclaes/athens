/**
 * 
 */
package com.co.core.auth.server.helper;

import com.co.core.identity.domain.Identity;

/**
 * @author Lucas Reeh
 *
 */
public class CurrentIdentity {

	private Identity identity;

	/**
	 * @return the identity
	 */
	public Identity getIdentity() {
		return identity;
	}

	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(Identity identity) {
		this.identity = identity;
	}
	
}
