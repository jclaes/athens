package com.co.core.auth.client;

import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Login Bar view
 * 
 * holds username and password field as well as login button
 * 
 * @author Lucas Reeh
 * 
 */
public class COLoginBarView extends ViewWithUiHandlers<COLoginBarUiHandlers>
		implements COLoginBarPresenter.MyView {

	private final Widget widget;

	public interface Binder extends UiBinder<Widget, COLoginBarView> {
	}

	@UiField
	TextField username;

	@UiField
	PasswordField password;

	@UiField
	TextButton btSignIn;

	/**
	 * Class constructor
	 * 
	 * @param binder
	 */
	@Inject
	public COLoginBarView(final Binder binder) {
		widget = binder.createAndBindUi(this);
	}

	/**
	 * @return view as widget
	 */
	@Override
	public Widget asWidget() {
		return widget;
	}

	/**
	 * user pressed key enter on password field
	 */
	@UiHandler("password")
	void onPasswordFieldEnterKeyPressed(KeyDownEvent event) {
		if (getUiHandlers() != null) {
			getUiHandlers().onPasswordFieldEnterKeyPressed(event);
		}
	}

	/**
	 * user pressed sign in button
	 */
	@UiHandler("btSignIn")
	void onSignInButtonPressed(SelectEvent event) {
		if (getUiHandlers() != null) {
			getUiHandlers().onSignInButtonPressed(event);
		}
	}

	/**
	 * @return username
	 */
	public TextField getUsername() {
		return username;
	}

	/**
	 * @return password
	 */
	public PasswordField getPassword() {
		return password;
	}

	/**
	 * @return btSignIn
	 */
	public TextButton getBtSignIn() {
		return btSignIn;
	}

}
