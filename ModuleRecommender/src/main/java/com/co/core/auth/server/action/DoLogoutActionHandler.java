package com.co.core.auth.server.action;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.gwtplatform.dispatch.server.actionhandler.ActionHandler;
import com.co.core.auth.client.action.DoLogout;
import com.co.core.auth.client.action.DoLogoutResult;
import com.co.core.auth.server.helper.CODBHelperSession;
import com.co.core.auth.shared.COClientUserModel;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.dispatch.server.ExecutionContext;
import com.gwtplatform.dispatch.shared.ActionException;

/**
 * Do Logout Action Handler class
 * 
 * handles {@link DoLogout} action
 * 
 * @author Lucas Reeh
 * 
 */
public class DoLogoutActionHandler implements
		ActionHandler<DoLogout, DoLogoutResult> {

	/**
	 * requestProvider
	 */
	private final Provider<HttpServletRequest> requestProvider;

	/**
	 * Class constructor
	 * 
	 * modified to get request provider
	 * 
	 * @param servletContext
	 * @param requestProvider
	 */
	@Inject
	public DoLogoutActionHandler(final ServletContext servletContext,
			final Provider<HttpServletRequest> requestProvider) {
		this.requestProvider = requestProvider;
	}

	/**
	 * log out user remove session attribute and send anonymous user back
	 */
	@Override
	public DoLogoutResult execute(DoLogout action, ExecutionContext context)
			throws ActionException {
		COClientUserModel coClientUser = new COClientUserModel();
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		HttpSession session = requestProvider.get().getSession();
		session.setAttribute("login.authenticated", null);
		coDBHelperSession.removeCurrentSession();
		return new DoLogoutResult(coClientUser);
	}

	/**
	 * undo
	 */
	@Override
	public void undo(DoLogout action, DoLogoutResult result,
			ExecutionContext context) throws ActionException {
	}

	/**
	 * @return action type
	 */
	@Override
	public Class<DoLogout> getActionType() {
		return DoLogout.class;
	}
}
