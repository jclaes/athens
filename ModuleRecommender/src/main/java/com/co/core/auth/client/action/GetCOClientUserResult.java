package com.co.core.auth.client.action;

import com.gwtplatform.dispatch.shared.Result;
import com.co.core.auth.shared.COClientUserModel;

/**
 * Result Class for {@link GetCOClientUser}
 * @author Lucas Reeh
 *
 */
public class GetCOClientUserResult implements Result {

	/**
	 * identified user from server
	 */
	private COClientUserModel coClientUser;

	/**
	 * Class constructor (no parameter constructor for serialization only)
	 */
	@SuppressWarnings("unused")
	private GetCOClientUserResult() {
	}

	/**
	 * Class constructor
	 * 
	 * @param coClientUser
	 */
	public GetCOClientUserResult(COClientUserModel coClientUser) {
		this.coClientUser = coClientUser;
	}

	/**
	 * @return {@link GetCOClientUserResult#coClientUser}
	 */
	public COClientUserModel getCoClientUser() {
		return coClientUser;
	}
}
