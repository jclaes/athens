/**
 * 
 */
package com.co.core.auth.shared;

import java.io.Serializable;

/**
 * TODO
 * 
 * Here is actually a lot of work to do for making code clean
 * 
 * Model should be removed and replaced with real object on server
 * 
 * 
 * @author Lucas Reeh
 *
 */
public class COClientUserModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5994427172242897017L;

	/**
	 * name for displaying name of current user
	 */
	private String displayName = "";
	
	/**
	 * is client user logged in
	 */
	private boolean loggedIn = false;
	
	/**
	 * id for client user
	 */
	private Long Id;
	
	/**
	 * locale settings for session
	 */
	private String locale = "";
	
	/**
	 * is client user student
	 */
	private boolean student = false;
	
	/**
	 * is client user staff
	 */
	private boolean staff = false;
	
	
	/**
	 * version for request factory
	 */
	private Integer version;
	
	
	/**
	 * Class empty constructor for serialization
	 */
	public COClientUserModel() {
	}
	
	/**
	 * Class constructor
	 * 
	 * @param displayName
	 */
	public COClientUserModel(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		Id = id;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	/**
	 * @return the loggedIn
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}

	/**
	 * @param loggedIn the loggedIn to set
	 */
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}

	/**
	 * @return the student
	 */
	public boolean isStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(boolean student) {
		this.student = student;
	}

	/**
	 * @return the staff
	 */
	public boolean isStaff() {
		return staff;
	}

	/**
	 * @param staff the staff to set
	 */
	public void setStaff(boolean staff) {
		this.staff = staff;
	}
	
}
