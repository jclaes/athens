package com.co.core.auth.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.EventHandler;
import com.co.core.auth.shared.COClientUserModel;
import com.google.gwt.event.shared.HasHandlers;

/**
 * User Loaded Event class (generated)
 * 
 * if user is loaded from the server
 * 
 * @author Lucas Reeh
 *
 */
public class COClientUserLoadedEvent extends
		GwtEvent<COClientUserLoadedEvent.COClientUserLoadedHandler> {

	public static Type<COClientUserLoadedHandler> TYPE = new Type<COClientUserLoadedHandler>();
	private COClientUserModel coClientUser;

	public interface COClientUserLoadedHandler extends EventHandler {
		void onCOClientUserLoaded(COClientUserLoadedEvent event);
	}

	public COClientUserLoadedEvent(COClientUserModel coClientUser) {
		this.coClientUser = coClientUser;
	}

	public COClientUserModel getCoClientUser() {
		return coClientUser;
	}

	@Override
	protected void dispatch(COClientUserLoadedHandler handler) {
		handler.onCOClientUserLoaded(this);
	}

	@Override
	public Type<COClientUserLoadedHandler> getAssociatedType() {
		return TYPE;
	}

	public static Type<COClientUserLoadedHandler> getType() {
		return TYPE;
	}

	public static void fire(HasHandlers source, COClientUserModel coClientUser) {
		source.fireEvent(new COClientUserLoadedEvent(coClientUser));
	}
}
