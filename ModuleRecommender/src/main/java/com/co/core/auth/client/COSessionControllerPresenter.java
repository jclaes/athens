package com.co.core.auth.client;

import com.co.core.auth.client.action.DoLogout;
import com.co.core.auth.client.action.DoLogoutResult;
import com.co.core.auth.client.event.COClientUserChangedEvent;
import com.co.core.auth.client.event.COClientUserLoadedEvent;
import com.co.core.auth.client.event.COClientUserChangedEvent.COClientUserChangedHandler;
import com.co.core.auth.client.event.COClientUserLoadedEvent.COClientUserLoadedHandler;
import com.co.core.auth.client.event.COClientUserLogsOutEvent;
import com.co.core.auth.client.event.COClientUserLogsOutEvent.COClientUserLogsOutHandler;
import com.co.core.auth.shared.COClientUserModel;
import com.co.core.auth.shared.resources.COAuthResources;
import com.co.core.shared.resources.COResources;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;
import com.google.inject.Inject;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.web.bindery.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.cell.core.client.ButtonCell.ButtonArrowAlign;
import com.sencha.gxt.cell.core.client.ButtonCell.IconAlign;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.info.Info;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuItem;

/**
 * Session Controller Presenter
 * 
 * presents split button with menu for current user to control session
 * attributes and logout and e.g. profile switch
 * 
 * @author Lucas Reeh
 * 
 */
public class COSessionControllerPresenter extends
		PresenterWidget<COSessionControllerPresenter.MyView> implements COSessionControllerUiHandlers {

	/**
	 * default view interface
	 * 
	 * @author Lucas Reeh
	 * 
	 */
	public interface MyView extends View, HasUiHandlers<COSessionControllerUiHandlers> {
		
		public TextButton getBtSessionController();

		public MenuItem getMiLogout();
		
		public Menu getSplitButtonMenu();
	}

	/**
	 * Class constructor
	 * 
	 * @param eventBus
	 * @param view
	 */
	@Inject
	public COSessionControllerPresenter(final EventBus eventBus,
			final MyView view) {
		super(eventBus, view);
		getView().setUiHandlers(this);
	}

	@Inject
	DispatchAsync dispatchAsynch;
	
	/**
	 * current user
	 */
	@Inject
	COClientUserHolder coClientUserHolder;

	/**
	 * handles {@link COClientUserLoadedEvent}
	 */
	public final COClientUserLoadedHandler coClientUserLoadedHandler = new COClientUserLoadedHandler() {
		@Override
		public void onCOClientUserLoaded(COClientUserLoadedEvent event) {
			setCOClientUserAndVisibility(event.getCoClientUser());
		}
	};

	/**
	 * handles {@link COClientUserChangedEvent}
	 */
	public final COClientUserChangedHandler coClientUserChangedHandler = new COClientUserChangedHandler() {
		@Override
		public void onCOClientUserChanged(COClientUserChangedEvent event) {
			setCOClientUserAndVisibility(event.getCoClientUser());
		}
	};

	/**
	 * Callback for log out on server
	 */
	private AsyncCallback<DoLogoutResult> doLogoutCallback = new AsyncCallback<DoLogoutResult>() {
		/**
		 * generates and shows AlertMessageBox
		 */
		@Override
		public void onFailure(Throwable caught) {
			AlertMessageBox alert = new AlertMessageBox(
					COResources.TEXT_ERROR.error(), caught.getLocalizedMessage());
			alert.show();
		}

		/**
		 * if result's user is logged in {@link COClientUserChangedEvent} is
		 * fired else info will be displayed
		 * 
		 * @param result
		 *            from server
		 */
		@Override
		public void onSuccess(DoLogoutResult result) {
			if (!result.getCoClientUser().isLoggedIn()) {
				coClientUserHolder.setUser(result.getCoClientUser());
				COClientUserChangedEvent coClientChangedEvent = new COClientUserChangedEvent(
						result.getCoClientUser());
				getEventBus().fireEvent(coClientChangedEvent);
			} else {
				coClientUserHolder.setUser(null);
				Info.display(COResources.TEXT_ERROR.error(),
						COAuthResources.TEXT.msgLogoutError());
			}
		}
	};

	/**
	 * handles {@link COClientUserLogsOutEvent}
	 */
	public final COClientUserLogsOutHandler coClientUserLogsOutHandler = new COClientUserLogsOutHandler() {

		/**
		 * if user logs out {@link DoLogout} action is executed
		 */
		@Override
		public void onCOClientUserLogsOut(COClientUserLogsOutEvent event) {
			DoLogout doLogoutAction = new DoLogout();
			dispatchAsynch.execute(doLogoutAction, doLogoutCallback);
		}

	};

	/**
	 * set coClientUser if user is logged in content in view will be set
	 * (username on split button) and visibility of button else it will be
	 * hidden
	 * 
	 * @param coClientUser
	 *            COClientUser
	 */
	public void setCOClientUserAndVisibility(COClientUserModel coClientUser) {
		if (coClientUser.isLoggedIn()) {
			getView().getBtSessionController().setText(
					coClientUser.getDisplayName());
			getView().getBtSessionController().setIcon(
					COAuthResources.IMAGES.user());
			getView().getBtSessionController().getMenu().setVisible(true);

		} else {
			getView().getBtSessionController().setText(COAuthResources.TEXT.anonymous());
			getView().getBtSessionController().getMenu().setVisible(false);
		}
	}

	/**
	 * bindings, registering of handlers and selection handlers of user interface elements
	 */
	@Override
	protected void onBind() {
		super.onBind();
		registerHandler(getEventBus().addHandler(
				COClientUserLoadedEvent.getType(), coClientUserLoadedHandler));
		registerHandler(getEventBus().addHandler(
				COClientUserChangedEvent.getType(), coClientUserChangedHandler));
		registerHandler(getEventBus().addHandler(
				COClientUserLogsOutEvent.getType(), coClientUserLogsOutHandler));
	}

	/**
	 * setting initial contents of view elements
	 */
	@Override
	protected void onReset() {
		super.onReset();
		getView().getBtSessionController().setIconAlign(IconAlign.LEFT);

		getView().getBtSessionController().setArrowAlign(ButtonArrowAlign.RIGHT);
		getView().getMiLogout().setText(COAuthResources.TEXT.logout());
		getView().getMiLogout().setIcon(COAuthResources.IMAGES.lock_16());

	}

	/**
	 * onLogoutSelected user interface event from view 
	 */
	@Override
	public void onLogoutSelected(SelectionEvent<Item> event) {
		COClientUserLogsOutEvent coClientUserLogsOutEvent = new COClientUserLogsOutEvent();
		getEventBus().fireEvent(coClientUserLogsOutEvent);
	}

	/**
	 * onSessionControllerButtonSelected user interface event from view 
	 */
	@Override
	public void onSessionControllerButtonSelected(SelectEvent event) {
		
	}

}
