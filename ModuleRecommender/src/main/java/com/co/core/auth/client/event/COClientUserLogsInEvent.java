package com.co.core.auth.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.EventHandler;
import java.lang.String;
import com.google.gwt.event.shared.HasHandlers;

/**
 * User Logs In Event class (generated)
 * 
 * if user presses sign in button
 * 
 * @author Lucas Reeh
 *
 */
public class COClientUserLogsInEvent extends
		GwtEvent<COClientUserLogsInEvent.COClientUserLogsInHandler> {

	public static Type<COClientUserLogsInHandler> TYPE = new Type<COClientUserLogsInHandler>();
	private String username;
	private String password;

	public interface COClientUserLogsInHandler extends EventHandler {
		void onCOClientUserLogsIn(COClientUserLogsInEvent event);
	}

	public COClientUserLogsInEvent(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	protected void dispatch(COClientUserLogsInHandler handler) {
		handler.onCOClientUserLogsIn(this);
	}

	@Override
	public Type<COClientUserLogsInHandler> getAssociatedType() {
		return TYPE;
	}

	public static Type<COClientUserLogsInHandler> getType() {
		return TYPE;
	}

	public static void fire(HasHandlers source, String username, String password) {
		source.fireEvent(new COClientUserLogsInEvent(username, password));
	}
}
