package com.co.core.auth.client.action;

import com.gwtplatform.dispatch.shared.ActionImpl;
import com.co.core.auth.client.action.DoLoginResult;

import java.lang.String;

/**
 * Login Action class
 * 
 * for sending credentials to server
 * 
 * @author Lucas Reeh
 * 
 */
public class DoLogin extends ActionImpl<DoLoginResult> {

	/**
	 * user name
	 */
	private String username;

	/**
	 * user password
	 */
	private String password;

	/**
	 * Class constructor (no parameter constructor for serialization only)
	 */
	@SuppressWarnings("unused")
	private DoLogin() {
	}

	/**
	 * Class constructor
	 * 
	 * @param username
	 * @param password
	 */
	public DoLogin(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * @return {@link DoLogin#username}
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return {@link DoLogin#password}
	 */
	public String getPassword() {
		return password;
	}
}
