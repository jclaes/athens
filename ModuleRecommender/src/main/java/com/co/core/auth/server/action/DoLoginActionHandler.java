package com.co.core.auth.server.action;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.gwtplatform.dispatch.server.actionhandler.ActionHandler;
import com.co.core.auth.client.action.DoLogin;
import com.co.core.auth.client.action.DoLoginResult;
import com.co.core.auth.server.helper.CODBHelperSession;
import com.co.core.auth.shared.COClientUserModel;
import com.co.core.identity.domain.Identity;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.dispatch.server.ExecutionContext;
import com.gwtplatform.dispatch.shared.ActionException;

/**
 * Do Login Action Handler class
 * 
 * handles {@link DoLogin} action
 * 
 * @author Lucas Reeh
 * 
 */
public class DoLoginActionHandler implements
		ActionHandler<DoLogin, DoLoginResult> {

	/**
	 * requestProvider
	 */
	private final Provider<HttpServletRequest> requestProvider;

	/**
	 * Class constructor
	 * 
	 * modified to get request provider
	 * 
	 * @param servletContext
	 * @param requestProvider
	 */
	@Inject
	public DoLoginActionHandler(final ServletContext servletContext,
			final Provider<HttpServletRequest> requestProvider) {
		this.requestProvider = requestProvider;
	}

	/**
	 * check user credentials and send {@link COClientUserModel} back to client
	 */
	@Override
	public DoLoginResult execute(DoLogin action, ExecutionContext context)
			throws ActionException {
		COClientUserModel coClientUser = new COClientUserModel();
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		Integer accountNr = coDBHelperSession.checkCredentials(
				action.getUsername(), action.getPassword());
		if (accountNr != null) {
			coDBHelperSession.initFakeSessionFromWeb(accountNr, this.requestProvider.get().getLocale());
			HttpSession session = requestProvider.get().getSession();
			session.setAttribute("login.authenticated", accountNr);
			coClientUser.setLoggedIn(true);
			Long identityNr = coDBHelperSession.getIdentitaetNr();
			Identity identity = Identity.find(identityNr);
			coClientUser.setDisplayName(identity.getFullName());
			coClientUser.setStaff(identity.isStaff());
			coClientUser.setStudent(identity.isStudent());
			return new DoLoginResult(coClientUser);
		}
		
		return new DoLoginResult(coClientUser);
	}

	/**
	 * undo action
	 */
	@Override
	public void undo(DoLogin action, DoLoginResult result,
			ExecutionContext context) throws ActionException {
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		HttpSession session = requestProvider.get().getSession();
		session.setAttribute("login.authenticated", null);
		coDBHelperSession.removeCurrentSession();
	}

	/**
	 * @return action type
	 */
	@Override
	public Class<DoLogin> getActionType() {
		return DoLogin.class;
	}
}
