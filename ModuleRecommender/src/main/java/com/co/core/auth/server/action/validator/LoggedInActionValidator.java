/**
 * 
 */
package com.co.core.auth.server.action.validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.co.core.auth.server.helper.CODBHelperSession;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.dispatch.server.actionvalidator.ActionValidator;
import com.gwtplatform.dispatch.shared.Action;
import com.gwtplatform.dispatch.shared.Result;

/**
 * Class for validation protected actions on Server
 * 
 * use in ServerModule as parameter
 * 
 * @author Lucas Reeh
 *
 */
public class LoggedInActionValidator implements ActionValidator {

	/**
	 * requestProvider
	 */
	private final Provider<HttpServletRequest> requestProvider;

	/**
	 * Class constructor
	 * 
	 * modified to get request provider
	 * 
	 * @param servletContext
	 * @param requestProvider
	 */
	@Inject
	LoggedInActionValidator(final Provider<HttpServletRequest> requestProvider) {
		
		
		this.requestProvider = requestProvider;
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		HttpSession session = requestProvider.get().getSession();
		Object accountNr = session.getAttribute("login.authenticated");
		if (accountNr != null) {
			coDBHelperSession.initFakeSessionFromWeb((Integer) accountNr, this.requestProvider.get().getLocale());
		}
	}
	
	/**
	 * @return true if user is logged in
	 */
	@Override
	public boolean isValid(Action<? extends Result> action){
		boolean result = true;
		
		HttpSession session = requestProvider.get().getSession();
		
		Object authenticated = session.getAttribute("login.authenticated");
		
		if (authenticated == null) {
			result = false;
		}
		
		return result;
	}

}
