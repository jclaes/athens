/**
 * 
 */
package com.co.core.auth.shared.service.proxy;

import com.co.core.auth.server.dao.COClientUser;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;

/**
 * @author Lucas Reeh
 *
 */
@ProxyFor(COClientUser.class)
public interface COClientUserProxy extends EntityProxy {
	
	Long getId();
	
	Integer getVersion();
	
	Boolean isLoggedIn();
	
	Boolean isStudent();
	
	Boolean isStaff();
	
	String getDisplayName();


}
