/**
 * 
 */
package com.co.core.auth.server.factory;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.co.core.auth.server.helper.CODBHelperSession;
import com.co.core.auth.server.helper.CurrentIdentity;
import com.co.core.identity.domain.Identity;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.gwtplatform.dispatch.server.Dispatch;
import com.gwtplatform.dispatch.server.RequestProvider;
import com.gwtplatform.dispatch.server.guice.DispatchServiceImpl;
import com.teklabs.gwt.i18n.server.LocaleProxy;

/**
 * @author Lucas Reeh
 *
 */
@Singleton
public class ActionInitUserServlet extends DispatchServiceImpl {

	private CurrentIdentity identity;
	/**
	 * 
	 */
	private static final long serialVersionUID = 2242911782284701053L;

	/**
	 * @param logger
	 * @param dispatch
	 * @param requestProvider
	 */
	@Inject
	public ActionInitUserServlet(Logger logger, Dispatch dispatch,
			RequestProvider requestProvider, CurrentIdentity identity) {
		super(logger, dispatch, requestProvider);
		this.identity = identity;
	}
	
	/**
	 * init session before for all services
	 */
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		initSession(req.getSession(), req);
		super.service(req, resp);
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		identity.setIdentity(new Identity());
		coDBHelperSession.removeCurrentSession();
	}
	
	/**
	 * init db session on co DB
	 * 
	 * @param session
	 */
	private void initSession(HttpSession session, ServletRequest req) {
                LocaleProxy.initialize();
                LocaleProxy.setLocale(req.getLocale());
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		HttpSession currentSession = session;
		Object accountNr = currentSession.getAttribute("login.authenticated");
                coDBHelperSession.setDataLanguageOnDB(coDBHelperSession.getCOLangCodeFromLocale(LocaleProxy.getLocale()));
                coDBHelperSession.setLanguageOnDB(coDBHelperSession.getCOLangCodeFromLocale(LocaleProxy.getLocale()));
		if (accountNr != null) {
			coDBHelperSession.initFakeSessionFromWeb((Integer) accountNr, req.getLocale());
			identity.setIdentity(Identity.find(coDBHelperSession.getIdentitaetNr()));
		} else {
			identity.setIdentity(new Identity());
			coDBHelperSession.removeCurrentSession();
		}
	}
	
	

}
