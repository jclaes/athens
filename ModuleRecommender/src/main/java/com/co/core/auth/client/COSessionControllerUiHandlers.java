/**
 * 
 */
package com.co.core.auth.client;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.gwtplatform.mvp.client.UiHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.menu.Item;

/**
 * Interface Session Controller user interface Handlers
 * 
 * interface holding all user interface Handlers of the Session Controller
 * 
 * @author Lucas Reeh
 *
 */
public interface COSessionControllerUiHandlers extends UiHandlers {
	
	/**
	 * user selected logout
	 */
	void onLogoutSelected(SelectionEvent<Item> event);
	
	/**
	 * user clicked session controller button
	 * 
	 * @param event
	 */
	void onSessionControllerButtonSelected(SelectEvent event);

}
