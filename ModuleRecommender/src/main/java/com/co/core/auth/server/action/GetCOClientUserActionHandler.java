package com.co.core.auth.server.action;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import com.gwtplatform.dispatch.server.actionhandler.ActionHandler;
import com.co.core.auth.client.action.GetCOClientUser;
import com.co.core.auth.client.action.GetCOClientUserResult;
import com.co.core.auth.server.helper.CODBHelperSession;
import com.co.core.auth.shared.COClientUserModel;
import com.co.core.identity.domain.Identity;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.gwtplatform.dispatch.server.ExecutionContext;
import com.gwtplatform.dispatch.shared.ActionException;

/**
 * Get COClientUser Action Handler class
 * 
 * handles {@link GetCOClientUser} action
 * 
 * @author Lucas Reeh
 *
 */
public class GetCOClientUserActionHandler implements
		ActionHandler<GetCOClientUser, GetCOClientUserResult> {

	/**
	 * requestProvider
	 */
	@SuppressWarnings("unused")
	private final Provider<HttpServletRequest> requestProvider;
	
	/**
	 * Class constructor
	 * 
	 * modified to get request provider
	 * 
	 * @param servletContext
	 * @param requestProvider
	 */
	@Inject
	public GetCOClientUserActionHandler(final ServletContext servletContext,
			final Provider<HttpServletRequest> requestProvider) {
		this.requestProvider = requestProvider;
	}

	/**
	 * returns the current {@link COClientUserModel}
	 */
	@Override
	public GetCOClientUserResult execute(GetCOClientUser action,
			ExecutionContext context) throws ActionException {
		COClientUserModel coClientUser = new COClientUserModel();
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		Long identityNr = coDBHelperSession.getIdentitaetNr();
		if (identityNr != null) {
			coClientUser.setLoggedIn(true);
			Identity identity = Identity.find(identityNr);
			coClientUser.setDisplayName(identity.getFullName());
			coClientUser.setStaff(identity.isStaff());
			coClientUser.setStudent(identity.isStudent());
		}

		return new GetCOClientUserResult(coClientUser);
	}

	/**
	 * undo
	 */
	@Override
	public void undo(GetCOClientUser action, GetCOClientUserResult result,
			ExecutionContext context) throws ActionException {
	}

	/**
	 * @return action type
	 */
	@Override
	public Class<GetCOClientUser> getActionType() {
		return GetCOClientUser.class;
	}
}
