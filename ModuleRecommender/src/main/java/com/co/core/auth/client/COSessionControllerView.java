package com.co.core.auth.client;

import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.menu.Item;
import com.sencha.gxt.widget.core.client.menu.Menu;
import com.sencha.gxt.widget.core.client.menu.MenuItem;

/**
 * Session Controller view
 * 
 * @author Lucas Reeh
 * 
 */
public class COSessionControllerView extends
		ViewWithUiHandlers<COSessionControllerUiHandlers> implements
		COSessionControllerPresenter.MyView {

	private final Widget widget;

	public interface Binder extends UiBinder<Widget, COSessionControllerView> {
	}

	/**
	 * split button ui element
	 */
	@UiField
	TextButton btSessionController;

	/**
	 * split button menu
	 */
	@UiField
	Menu splitButtonMenu;
	
	/**
	 * menu item for logout
	 */
	@UiField
	MenuItem miLogout;

	/**
	 * Class constructor
	 * 
	 * @param binder
	 */
	@Inject
	public COSessionControllerView(final Binder binder) {
		widget = binder.createAndBindUi(this);
	}

	/**
	 * returns view as widget
	 */
	@Override
	public Widget asWidget() {
		return widget;
	}

	@UiHandler("miLogout")
	void onLogoutSelected(SelectionEvent<Item> event) {
		if (getUiHandlers() != null) {
			getUiHandlers().onLogoutSelected(event);
		}
	}
	
	@UiHandler("btSessionController")
	void onSessionControllerButtonSelected(SelectEvent event) {
		if (getUiHandlers() != null) {
			getUiHandlers().onSessionControllerButtonSelected(event);
		}
	}

	/**
	 * @return the {@link COSessionControllerView#btSessionController}
	 */
	public TextButton getBtSessionController() {
		return btSessionController;
	}

	/**
	 * @return the {@link COSessionControllerView#miLogout}
	 */
	public MenuItem getMiLogout() {
		return miLogout;
	}

	/**
	 * @return the {@link COSessionControllerView#splitButtonMenu}
	 */
	public Menu getSplitButtonMenu() {
		return splitButtonMenu;
	}
}
