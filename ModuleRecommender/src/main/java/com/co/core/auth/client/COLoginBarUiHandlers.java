/**
 * 
 */
package com.co.core.auth.client;

import com.google.gwt.event.dom.client.KeyDownEvent;
import com.gwtplatform.mvp.client.UiHandlers;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

/**
 * Interface Login Bar user interface Handlers
 * 
 * interface holding all user interface Handlers of the Login Bar
 * 
 * @author Lucas Reeh
 *
 */
public interface COLoginBarUiHandlers extends UiHandlers {

	/**
	 * user pressed key enter on password field
	 */
	void onPasswordFieldEnterKeyPressed(KeyDownEvent event);
	
	/**
	 * user pressed sign in button
	 */
	void onSignInButtonPressed(SelectEvent event);
	
}
