/**
 * 
 */
package com.co.core.auth.shared.service;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

/**
 * Request Factory Interface for Auth Data
 * 
 * @author Lucas Reeh
 *
 */
public interface COAuthDataRequestFactory extends RequestFactory {

	COClientUserRequest getCOClientUserRequest();
	
}
