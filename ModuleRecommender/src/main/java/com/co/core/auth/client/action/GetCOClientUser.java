package com.co.core.auth.client.action;

import com.gwtplatform.dispatch.shared.ActionImpl;
import com.co.core.auth.client.action.GetCOClientUserResult;

/**
 * Get COClientUser Action class
 * 
 * returns logged in user from server
 * 
 * @author Lucas Reeh
 *
 */
public class GetCOClientUser extends ActionImpl<GetCOClientUserResult> {

	/**
	 * Class constructor
	 */
	public GetCOClientUser() {
	}
}
