package com.co.core.auth.client;

import com.co.core.auth.client.action.DoLogin;
import com.co.core.auth.client.action.DoLoginResult;
import com.co.core.auth.client.event.COClientUserChangedEvent;
import com.co.core.auth.client.event.COClientUserLogsInEvent;
import com.co.core.auth.client.event.COClientUserLogsInEvent.COClientUserLogsInHandler;
import com.co.core.auth.shared.resources.COAuthResources;
import com.co.core.shared.resources.COResources;
import com.google.gwt.core.client.GWT;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.PresenterWidget;
import com.gwtplatform.mvp.client.View;
import com.google.inject.Inject;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.web.bindery.event.shared.EventBus;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.cell.core.client.ButtonCell.IconAlign;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.info.Info;

/**
 * Login Bar Presenter
 * 
 * presents username and password fields for login (used in header presenter)
 * 
 * @author Lucas Reeh
 *
 */
public class COLoginBarPresenter extends
		PresenterWidget<COLoginBarPresenter.MyView> implements COLoginBarUiHandlers {

	/**
	 * default view interface
	 * 
	 * @author Lucas Reeh
	 * 
	 */
	public interface MyView extends View, HasUiHandlers<COLoginBarUiHandlers> {

		public PasswordField getPassword();

		public TextField getUsername();

		public TextButton getBtSignIn();
	}

	/**
	 * Class constructor
	 * 
	 * @param eventBus
	 * @param view
	 */
	@Inject
	public COLoginBarPresenter(final EventBus eventBus, final MyView view) {
		super(eventBus, view);
		getView().setUiHandlers(this);
	}

	/**
	 * DispatchAsnch interface for lunching call backs
	 */
	@Inject
	DispatchAsync dispatchAsynch;

	/**
	 * current user
	 */
	@Inject
	COClientUserHolder coClientUserHolder;
	
	/**
	 * call back for user login
	 * 
	 * fires {@link COClientUserChangedEvent} on successful login
	 * 
	 */
	private AsyncCallback<DoLoginResult> doLoginCallback = new AsyncCallback<DoLoginResult>() {

		@Override
		public void onFailure(Throwable caught) {
			AlertMessageBox alert = new AlertMessageBox(
					COResources.TEXT_ERROR.error(), caught.getLocalizedMessage());
			alert.show();
            GWT.log(caught.getStackTrace().toString()+" "+caught.getMessage());
			resetFieldsAndFocus();
		}

		@Override
		public void onSuccess(DoLoginResult result) {
			if (result.getCoClientUser().isLoggedIn()) {
				coClientUserHolder.setUser(result.getCoClientUser());
				COClientUserChangedEvent coClieChangedEvent = new COClientUserChangedEvent(
						result.getCoClientUser());
				getEventBus().fireEvent(coClieChangedEvent);
			} else {
				Info.display(COResources.TEXT_ERROR.error(),
						COAuthResources.TEXT.msgUserPasswordIncorrect());
				coClientUserHolder.setUser(null);
				resetFieldsAndFocus();
			}
		}
	};

	/**
	 * handles {@link COClientUserLogsInEvent} and executes {@link DoLogin} action
	 * 
	 * TODO write FieldVerifier in separate class
	 * 
	 */
	public final COClientUserLogsInHandler coClientUserLogsInHandler = new COClientUserLogsInHandler() {

		@Override
		public void onCOClientUserLogsIn(COClientUserLogsInEvent event) {
			if ("".equals(event.getPassword())) {
				Window.alert(COAuthResources.TEXT.msgErrorInLoginDataForm());
				return;
			}
			if ("".equals(event.getUsername())) {
				Window.alert(COAuthResources.TEXT.msgErrorInLoginDataForm());
				return;
			}
			DoLogin doLoginAction = new DoLogin(event.getUsername(),
					event.getPassword());
			dispatchAsynch.execute(doLoginAction, doLoginCallback);
		}

	};

	/**
	 * onBind
	 * 
	 * binding handlers
	 * 
	 */
	@Override
	protected void onBind() {
		super.onBind();
		/* registering Handlers */
		registerHandler(getEventBus().addHandler(
				COClientUserLogsInEvent.getType(), coClientUserLogsInHandler));
	}

	/**
	 * fires {@link COClientUserLogsInEvent}
	 */
	private void fireUserLogsInEvent(){
		COClientUserLogsInEvent coClientUserLogsInEvent = new COClientUserLogsInEvent(
				getUserNameValue(), getPasswordValue());
		getEventBus().fireEvent(coClientUserLogsInEvent);
	}
	
	/**
	 * setting view contents
	 */
	@Override
	protected void onReset() {
		super.onReset();
		getView().getUsername().setEmptyText(COAuthResources.TEXT.username());
		getView().getPassword().setEmptyText(COAuthResources.TEXT.password());
		getView().getBtSignIn().setText(COAuthResources.TEXT.signIn());
		getView().getBtSignIn().setIcon(COAuthResources.IMAGES.key_16());
		getView().getBtSignIn().setIconAlign(IconAlign.LEFT);
	}

	/**
	 * onReveal
	 */
	@Override
	protected void onReveal() {
		super.onReveal();

	}

	/**
	 * @return value of username input box
	 */
	@Inject
	private String getUserNameValue() {
		return getView().getUsername().getValue();
	}

	/**
	 * @return value of password input box
	 */
	@Inject
	private String getPasswordValue() {
		return getView().getPassword().getValue();
	}

	/**
	 * resets username and password input box and sets focus on username field
	 */
	@Inject
	private void resetFieldsAndFocus() {
		getView().getUsername().setText("");
		getView().getPassword().setText("");
		getView().getUsername().focus();
	}

	/**
	 * onPasswordFieldEnterKeyPressed ui event from view 
	 */
	@Override
	public void onPasswordFieldEnterKeyPressed(KeyDownEvent event) {
		if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
			getView().getBtSignIn().focus();
			getView().getPassword().finishEditing();
			fireUserLogsInEvent();
		}
	}

	/**
	 * onSignInButtonPressed ui event from view 
	 */
	@Override
	public void onSignInButtonPressed(SelectEvent event) {
		fireUserLogsInEvent();
	}
	

}
