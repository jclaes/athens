package com.co.core.auth.client.action;

import com.co.core.auth.shared.COClientUserModel;
import com.gwtplatform.dispatch.shared.Result;

/**
 * Result Class for {@link DoLogin}
 * @author Lucas Reeh
 *
 */
public class DoLoginResult implements Result {

	private COClientUserModel coClientUser;

	/**
	 * Class constructor (no parameter constructor for serialization only)
	 */
	@SuppressWarnings("unused")
	private DoLoginResult() {
		// For serialization only
	}

	/**
	 * Class constructor
	 * 
	 * @param coClientUser
	 */
	public DoLoginResult(COClientUserModel coClientUser) {
		this.coClientUser = coClientUser;
	}

	/**
	 * @return {@link DoLoginResult#coClientUser}
	 */
	public COClientUserModel getCoClientUser() {
		return coClientUser;
	}
}
