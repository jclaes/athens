package com.co.core.auth.client.action;

import com.gwtplatform.dispatch.shared.Result;
import com.co.core.auth.shared.COClientUserModel;

/**
 * Result Class for {@link DoLogout}
 * @author Lucas Reeh
 *
 */
public class DoLogoutResult implements Result {

	/**
	 * anonymous user from server after log out
	 */
	private COClientUserModel coClientUser;

	/**
	 * Class constructor (no parameter constructor for serialization only)
	 */
	@SuppressWarnings("unused")
	private DoLogoutResult() {
		// For serialization only
	}

	/**
	 * Class constructor
	 * 
	 * @param coClientUser
	 */
	public DoLogoutResult(COClientUserModel coClientUser) {
		this.coClientUser = coClientUser;
	}

	/**
	 * @return {@link DoLogoutResult#coClientUser}
	 */
	public COClientUserModel getCoClientUser() {
		return coClientUser;
	}
}
