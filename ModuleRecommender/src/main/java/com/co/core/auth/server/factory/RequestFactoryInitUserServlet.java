/**
 * 
 */
package com.co.core.auth.server.factory;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.co.core.auth.server.helper.CODBHelperSession;
import com.co.core.auth.server.helper.CurrentIdentity;
import com.co.core.identity.domain.Identity;
import com.co.core.server.util.exception.LoquaciousExceptionHandler;
import com.co.server.guice.requestfactory.inject.InjectingServiceLayerDecorator;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;
import com.teklabs.gwt.i18n.server.LocaleProxy;

/**
 * Class for overriding request factory servlet and init co db session
 * 
 * @author Lucas Reeh
 * 
 */
@Singleton
public class RequestFactoryInitUserServlet extends RequestFactoryServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5871616687389494489L;
	

	private CurrentIdentity identity;
	
	/**
	 */
	@Inject
	public RequestFactoryInitUserServlet(InjectingServiceLayerDecorator serviceLayerDecorator, CurrentIdentity identity) {
		super(new LoquaciousExceptionHandler(), serviceLayerDecorator);
		this.identity = identity;
	}

	/**
	 * init session before for all services
	 */
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		initSession(req.getSession(), req);
		super.service(req, resp);
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		identity.setIdentity(new Identity());
		coDBHelperSession.removeCurrentSession();
	}

	/**
	 * init db session on co DB
	 * 
	 * @param session
	 */
	private void initSession(HttpSession session, ServletRequest req) {
                LocaleProxy.initialize();
                LocaleProxy.setLocale(req.getLocale());
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		HttpSession currentSession = session;
		Object accountNr = currentSession.getAttribute("login.authenticated");
		coDBHelperSession.setDataLanguageOnDB(coDBHelperSession.getCOLangCodeFromLocale(LocaleProxy.getLocale()));
                coDBHelperSession.setLanguageOnDB(coDBHelperSession.getCOLangCodeFromLocale(LocaleProxy.getLocale()));
		if (accountNr != null) {
			coDBHelperSession.initFakeSessionFromWeb((Integer) accountNr, req.getLocale());
			identity.setIdentity(Identity.find(coDBHelperSession.getIdentitaetNr()));
		} else {
			identity.setIdentity(new Identity());
			coDBHelperSession.removeCurrentSession();
		}
	}

}
