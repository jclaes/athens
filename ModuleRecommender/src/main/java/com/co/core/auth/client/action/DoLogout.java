package com.co.core.auth.client.action;

import com.gwtplatform.dispatch.shared.ActionImpl;
import com.co.core.auth.client.action.DoLogoutResult;

/**
 * Do Logout Action class
 * 
 * for log out user on the server
 * 
 * @author Lucas Reeh
 *
 */
public class DoLogout extends ActionImpl<DoLogoutResult> {

	/**
	 * Class constructor
	 */
	public DoLogout() {
	}
}
