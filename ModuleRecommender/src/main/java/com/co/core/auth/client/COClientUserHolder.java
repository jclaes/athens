/**
 * 
 */
package com.co.core.auth.client;

import com.co.core.auth.shared.COClientUserModel;
import com.co.core.auth.shared.resources.COAuthResources;
import com.google.inject.Inject;

/**
 * Class for holding current user
 * 
 * @author Lucas Reeh
 *
 */
public class COClientUserHolder {

	private COClientUserModel user = new COClientUserModel();

	/**
	 * Class constructor
	 */
	@Inject
	public COClientUserHolder() {
	}

	/**
	 * @return the user
	 */
	public COClientUserModel getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(COClientUserModel user) {
		if (user != null) {
			this.user = user;
		} else {
			COClientUserModel coClientUserModel = new COClientUserModel();
			coClientUserModel.setDisplayName(COAuthResources.TEXT.anonymous());
    		coClientUserModel.setLoggedIn(false);
    		this.user = coClientUserModel;
		}
		
	}
	
	

}
