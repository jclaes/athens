/**
 * 
 */
package com.co.core.auth.server.dao;

import com.co.core.auth.server.helper.CODBHelperSession;
import com.co.core.identity.domain.Identity;

/**
 * @author Lucas Reeh
 *
 */
public class COClientUser {
	
	/**
	 * name for displaying name of current user
	 */
	private String displayName;
	
	/**
	 * is client user logged in
	 */
	private Boolean loggedIn = false;
	
	/**
	 * is client student
	 */
	private Boolean student = false;
	
	/**
	 * is client staff
	 */
	private Boolean staff = false;
	
	/**
	 * id for client user
	 */
	private Long Id;
	
	
	/**
	 * version for request factory
	 */
	private Integer version;
	
	
	/**
	 * Class empty constructor for serialization
	 */
	public COClientUser() {
	}
	
	/**
	 * Class constructor
	 * 
	 * @param displayName
	 */
	public COClientUser(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		Id = id;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	/**
	 * @return the loggedIn
	 */
	public Boolean isLoggedIn() {
		return loggedIn;
	}

	/**
	 * @param loggedIn the loggedIn to set
	 */
	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	/**
	 * return current user from session id
	 */
	public static COClientUser getCurrentCOClientUser() {
		COClientUser coClientUser = new COClientUser();
		CODBHelperSession coDBHelperSession = new CODBHelperSession();
		Long identityNr = coDBHelperSession.getIdentitaetNr();
		if (coDBHelperSession.getIdentitaetNr() != null) {
			coClientUser.setLoggedIn(true);
			Identity identity = Identity.find(identityNr);
			coClientUser.setDisplayName(identity.getFullName());
			coClientUser.setStaff(identity.isStaff());
			coClientUser.setStudent(identity.isStudent());
		}

		return coClientUser;
	}
	
	/**
	 * @return true if is staff
	 */
	public Boolean isStaff() {
		return staff;
	}
	
	
	/**
	 * @return true if is staff
	 */
	public Boolean isStudent() {
		return student;
	}

	public void setStudent(Boolean student) {
		this.student = student;
	}

	public void setStaff(Boolean staff) {
		this.staff = staff;
	}

}
