package com.co.core.auth.client.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.EventHandler;
import com.co.core.auth.shared.COClientUserModel;
import com.google.gwt.event.shared.HasHandlers;

/**
 * User Changed Event class (generated)
 * 
 * if user is changed on the client
 * 
 * @author Lucas Reeh
 *
 */
public class COClientUserChangedEvent extends
		GwtEvent<COClientUserChangedEvent.COClientUserChangedHandler> {

	public static Type<COClientUserChangedHandler> TYPE = new Type<COClientUserChangedHandler>();
	private COClientUserModel coClientUser;

	public interface COClientUserChangedHandler extends EventHandler {
		void onCOClientUserChanged(COClientUserChangedEvent event);
	}

	public COClientUserChangedEvent(COClientUserModel coClientUser) {
		this.coClientUser = coClientUser;
	}

	public COClientUserModel getCoClientUser() {
		return coClientUser;
	}

	@Override
	protected void dispatch(COClientUserChangedHandler handler) {
		handler.onCOClientUserChanged(this);
	}

	@Override
	public Type<COClientUserChangedHandler> getAssociatedType() {
		return TYPE;
	}

	public static Type<COClientUserChangedHandler> getType() {
		return TYPE;
	}

	public static void fire(HasHandlers source, COClientUserModel coClientUser) {
		source.fireEvent(new COClientUserChangedEvent(coClientUser));
	}
}
