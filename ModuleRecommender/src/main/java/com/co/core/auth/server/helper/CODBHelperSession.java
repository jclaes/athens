/**
 * 
 */
package com.co.core.auth.server.helper;

import java.util.Locale;

import com.co.core.identity.domain.Identity;
import com.google.inject.Inject;


/**
 * Class with helper utils for session on CO database
 * 
 * TODO remove if session handling processed on other server
 * 
 * @author Lucas Reeh
 *
 */
public class CODBHelperSession {
	

	@Inject
	CurrentIdentity currentIdentity = new CurrentIdentity();
	
	public Integer checkCredentials(String username, String password) {
		if (username.equals("test1")) {
			return 1;
		} else if (username.equals("test2")) {
			return 2;
		}
		return null;
	}
	
	
	/**
	 * remove current session on co db
	 */
	public void removeCurrentSession() {
		this.currentIdentity = null;
	}
	
	/**
	 * remove current session on co db
	 */
	private void resetPackageState() {
		
	}
		
	
	/**
	 * create session on co db
	 * @param accountNr
	 */
	public void initFakeSessionFromWeb(Integer accountNr, Locale locale) {
		Identity ident = new Identity();
		if (accountNr == 1) {
			ident.setFristName("First Name1");
			ident.setLastName("Last Name1");
			ident.setId(new Long(1));
			ident.setStudentId(new Long(1));
		} else if (accountNr == 2) {
			ident.setFristName("First Name2");
			ident.setLastName("Last Name2");
			ident.setId(new Long(2));
			ident.setStudentId(new Long(2));
		}
		currentIdentity.setIdentity(ident);
	}
	
	/**
	 * calls setDataLanguage call on DB
	 * 
	 * @param string
	 */
	public void setDataLanguageOnDB(String string) {
		
	}
	
	/**
	 * calls setLanguage call on DB
	 * 
	 * @param string
	 */
	public void setLanguageOnDB(String string) {
		
	}


	/**
	 * return puSession.getIdentitaetNr
	 * @return
	 */
	public Long getIdentitaetNr() {
		if (this.currentIdentity != null) {
			if (this.currentIdentity.getIdentity() != null) {
				return this.currentIdentity.getIdentity().getId();
			}
		}
		return null;
	}
	
	/**
	 * @return puSession.getPersonGruppe
	 */
	public Integer getPersonGruppeNr() {
		if (currentIdentity.getIdentity().getId().equals(1)) {
			return 1;
		} else if (currentIdentity.getIdentity().getId().equals(2)) {
			return 2;
		}
		return null;
	}
	
	
	/**
	 * get CO langcode from Locale
	 */
	public String getCOLangCodeFromLocale(Locale locale) {
		return locale.getLanguage().toUpperCase();
	}
	
	

	
}
