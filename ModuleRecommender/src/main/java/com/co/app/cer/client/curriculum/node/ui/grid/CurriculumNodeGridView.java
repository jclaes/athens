package com.co.app.cer.client.curriculum.node.ui.grid;

import com.co.app.cer.client.curriculum.node.AttendanceChartCell;
import com.co.app.cer.client.curriculum.node.ButtonCellCustomAppearance;
import com.co.app.cer.client.curriculum.node.CurriculumNodeProxySimpleProperties;
import com.co.app.cer.client.curriculum.node.RecommendedByLecturerChartCell;
import com.co.app.cer.client.curriculum.node.RecommendedChartCell;
import com.co.app.cer.shared.resources.COCerResources;
import com.co.app.modrec.shared.proxy.CurriculumNodeProxy;
import com.co.app.modrec.shared.requestfactory.CurriculumRequestFactory;
import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.co.core.auth.client.COClientUserHolder;
import com.co.core.auth.shared.COClientUserModel;
import com.co.core.page.shared.resources.COPageResources;
import com.co.core.shared.Flag;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.sencha.gxt.cell.core.client.TextButtonCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.Store.StoreSortInfo;
import com.sencha.gxt.dnd.core.client.GridDragSource;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.button.ToggleButton;
import com.sencha.gxt.widget.core.client.container.BorderLayoutContainer.BorderLayoutData;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor.DoublePropertyEditor;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GroupingView;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import com.sencha.gxt.widget.core.client.grid.filters.NumericFilter;
import com.sencha.gxt.widget.core.client.grid.filters.StringFilter;
import com.sencha.gxt.widget.core.client.tips.QuickTip;

import javax.inject.Provider;
import java.util.ArrayList;
import java.util.List;

/**
 * Curriculum Node Grid View
 * <p/>
 * holds grid with curriculum nodes and attendance values
 *
 * @author Lucas Reeh
 */
public class CurriculumNodeGridView extends
        ViewWithUiHandlers<CurriculumNodeGridUiHandlers> implements
        CurriculumNodeGridPresenter.MyView {

    /**
     * key provider for tree grid
     */
    class KeyProvider implements ModelKeyProvider<CurriculumNodeProxy> {
        public String getKey(CurriculumNodeProxy item) {
            return item.getId().toString();
        }
    }

    private final Widget widget;

    public interface Binder extends UiBinder<Widget, CurriculumNodeGridView> {
    }

    @UiField(provided = true)
    BorderLayoutData northData = new BorderLayoutData(35);

    @UiField(provided = true)
    MarginData centerData = new MarginData();

    @UiField
    ToggleButton btAttendance;

    @UiField
    ToggleButton btAttendanceInactive;

    @UiField
    TextButton btRecommended;

    @UiField
    TextButton btRecommendedByLecturer;

    @UiField
    TextButton btAttendedModules;

    @UiField
    ToggleButton btSemesterType;

    @UiField
    ToggleButton btSemester;

    /**
     * creating properties class
     */
    CurriculumNodeProxySimpleProperties props = GWT
            .create(CurriculumNodeProxySimpleProperties.class);

    ListStore<CurriculumNodeProxy> store;

    /**
     * @return the store
     */
    public ListStore<CurriculumNodeProxy> getStore() {
        return store;
    }

    ColumnModel<CurriculumNodeProxy> cm;

    Grid<CurriculumNodeProxy> grid;

    GroupingView<CurriculumNodeProxy> groupingView;

    CurriculumRequestFactory factory;

    final AbstractImagePrototype imgBook16Prototype = AbstractImagePrototype
            .create(COModuleRecommenderResources.IMAGES.book_16());

    final Image imageBook16 = imgBook16Prototype.createImage();

    private final static Image recImage = AbstractImagePrototype.create(
            COModuleRecommenderResources.IMAGES.brick()).createImage();

    private final static Image infoImageAddRecommendation = AbstractImagePrototype.create(
            COModuleRecommenderResources.IMAGES.info_16()).createImage();

    private final static Image infoImageEditRecommendation = AbstractImagePrototype.create(
            COModuleRecommenderResources.IMAGES.info_16()).createImage();

    private final static Image infoImageViewRecommendation = AbstractImagePrototype.create(
            COModuleRecommenderResources.IMAGES.info_16()).createImage();

    private final static ImageResource recEditIcon = COModuleRecommenderResources.IMAGES
            .brick_edit();// .createImage();
    private final static ImageResource recViewIcon = COModuleRecommenderResources.IMAGES
            .brick();// .createImage();
    private final static ImageResource recAddIcon = COModuleRecommenderResources.IMAGES
            .brick_add();// .createImage();

    final SafeHtml htmlOrangeFlagIcon = SafeHtmlUtils
            .fromTrustedString(AbstractImagePrototype.create(
                    COCerResources.IMAGES.flag_orange()).getHTML());

    @UiField
    ContentPanel pnList;

    ColumnConfig<CurriculumNodeProxy, Double> colAttendanceActive = new ColumnConfig<CurriculumNodeProxy, Double>(
            props.attendance());

    ColumnConfig<CurriculumNodeProxy, Double> colAttendanceInactive = new ColumnConfig<CurriculumNodeProxy, Double>(
            props.attendanceInactive());

    ColumnConfig<CurriculumNodeProxy, String> colSemesterTypeRefId = new ColumnConfig<CurriculumNodeProxy, String>(
            props.semesterTypeId());

    ColumnConfig<CurriculumNodeProxy, Double> colSemester = new ColumnConfig<CurriculumNodeProxy, Double>(
            props.semester());

    ColumnConfig<CurriculumNodeProxy, Boolean> colRecommended = new ColumnConfig<CurriculumNodeProxy, Boolean>(
            props.recommended());

    ColumnConfig<CurriculumNodeProxy, Boolean> colRecommendedByLecturer = new ColumnConfig<CurriculumNodeProxy, Boolean>(
            props.recommendedByLecturer());

    ColumnConfig<CurriculumNodeProxy, String> colEditRecommendedByLecturer = new ColumnConfig<CurriculumNodeProxy, String>(
            props.hasRecommendations());

    ColumnConfig<CurriculumNodeProxy, String> colSuperNodeName = new ColumnConfig<CurriculumNodeProxy, String>(
            props.superNodeName());


    List<ColumnConfig<CurriculumNodeProxy, ?>> columnList = new ArrayList<ColumnConfig<CurriculumNodeProxy, ?>>();

    COClientUserModel user;

    TextButtonCell btEditRecommendation;

    /**
     * Class constructor
     *
     * @param binder
     */
    @Inject
    public CurriculumNodeGridView(final Binder binder,
                                  Provider<CurriculumRequestFactory> provider,
                                  COClientUserHolder userHolder) {
        // set locale vars
        this.factory = provider.get();
        this.user = userHolder.getUser();


        // init store and add sort
        store = new ListStore<CurriculumNodeProxy>(props.id());
        store.addSortInfo(new StoreSortInfo<CurriculumNodeProxy>(
                props.nodeId(), SortDir.DESC));

        // column for node icon
        ColumnConfig<CurriculumNodeProxy, Long> colIcon = new ColumnConfig<CurriculumNodeProxy, Long>(
                props.nodeId());
        colIcon.setCell(new AbstractCell<Long>() {
            @Override
            public void render(Context context, Long value, SafeHtmlBuilder sb) {
                sb.append(htmlOrangeFlagIcon);
            }

        });
        colIcon.setWidth(30);
        colIcon.setGroupable(false);
        colIcon.setSortable(false);
        colIcon.setFixed(true);
        colIcon.setMenuDisabled(true);

        // column for link to CO
        ColumnConfig<CurriculumNodeProxy, Long> colLinkCO = new ColumnConfig<CurriculumNodeProxy, Long>(
                props.nodeId());
        // tooltip
        imageBook16.setTitle(COModuleRecommenderResources.TEXT.linkToCO(COPageResources.TEXT.customTitle()));
        colLinkCO.setCell(new AbstractCell<Long>() {
            public void render(Context context, Long value, SafeHtmlBuilder sb) {
                String anchor = "";
                int row = context.getIndex();
                if (getStore().get(row).getModulePublic() != null) {
                    if (getStore().get(row).getModulePublic().equals(Flag.TRUE)) {
                        anchor = "<a href=\"https://campus.tum.de/tumonline/wbStpModHB.detailPage?pKnotenNr="
                                + value.toString() + "\"";
                        anchor += " target=\"_blank\">" + imageBook16.toString()
                                + "</a>";

                    }
                }
                sb.append(SafeHtmlUtils.fromTrustedString(anchor));
            }

        });
        colLinkCO.setWidth(30);
        colLinkCO.setGroupable(false);
        colLinkCO.setSortable(false);
        colLinkCO.setFixed(true);
        colLinkCO.setMenuDisabled(true);
        colLinkCO.setToolTip(SafeHtmlUtils.fromTrustedString(COModuleRecommenderResources.TEXT.linkToCO(COPageResources.TEXT.customTitle())));

        // column node name
        ColumnConfig<CurriculumNodeProxy, String> colName = new ColumnConfig<CurriculumNodeProxy, String>(
                props.name());
        colName.setHeader(COCerResources.TEXT.moduleName());
        colName.setWidth(400);
        colName.setGroupable(false);

        // column node short name (module id)
        ColumnConfig<CurriculumNodeProxy, String> colModuleId = new ColumnConfig<CurriculumNodeProxy, String>(
                props.nodeShortName());
        colModuleId.setGroupable(false);
        colModuleId.setWidth(80);
        colModuleId.setFixed(true);
        colModuleId.setHeader(COCerResources.TEXT.moduleId());
        colModuleId.setCell(new AbstractCell<String>() {
            @Override
            public void render(Context context, String value, SafeHtmlBuilder sb) {
                if (value != null)
                    sb.appendHtmlConstant("<span style=\"font-weight:bold;\">"
                            + value + "</span>");
            }
        });
        colModuleId.setToolTip(SafeHtmlUtils.fromTrustedString(COCerResources.TEXT.moduleIdLong()));

        // column super node name (for grouping)

        colSuperNodeName.setHeader(COCerResources.TEXT.context());
        colSuperNodeName.setGroupable(true);

        // column attendance value
        colAttendanceActive.setHeader(COModuleRecommenderResources.TEXT
                .attendancePercentage());
        colAttendanceActive.setWidth(130);
        colAttendanceActive.setResizable(false);
        colAttendanceActive.setFixed(true);
        AttendanceChartCell attendanceActiveCell = new AttendanceChartCell();
        attendanceActiveCell.setColor("213, 70, 121");
        colAttendanceActive.setCell(attendanceActiveCell);
        colAttendanceActive.setGroupable(false);

        // column attendance inactive students
        colAttendanceInactive.setHeader(COModuleRecommenderResources.TEXT
                .attendanceInactivePercentage());
        colAttendanceInactive.setWidth(130);
        colAttendanceInactive.setResizable(false);
        colAttendanceInactive.setFixed(true);
        AttendanceChartCell attendanceInactiveCell = new AttendanceChartCell();
        attendanceInactiveCell.setColor("36, 173, 154");
        colAttendanceInactive.setCell(attendanceInactiveCell);
        colAttendanceInactive.setGroupable(false);

        // column recommended semester
        colSemesterTypeRefId.setFixed(true);
        colSemesterTypeRefId.setGroupable(false);
        colSemesterTypeRefId.setWidth(30);
        colSemesterTypeRefId.setCell(new AbstractCell<String>() {
            @Override
            public void render(Context context, String value, SafeHtmlBuilder sb) {
                if (value != null)
                    sb.appendHtmlConstant("<span style=\"color:rgb(0, 132, 200);font-weight:bold;\">"
                            + value + "</span>");
            }
        });
        colSemesterTypeRefId.setHeader(COModuleRecommenderResources.TEXT
                .recommendedSemsterShort());
        colSemesterTypeRefId.setToolTip(SafeHtmlUtils
                .fromString(COModuleRecommenderResources.TEXT.recommendedSemster()));

        // column semester attended by students
        colSemester.setFixed(true);
        colSemester.setGroupable(false);
        colSemester.setWidth(30);
        colSemester.setCell(new AbstractCell<Double>() {
            public void render(Context context, Double value,
                               SafeHtmlBuilder sb) {
                if (value != null)
                    sb.appendHtmlConstant("<span style=\"color:rgb(97, 186, 61);font-weight:bold;\">"
                            + value.intValue() + "." + "</span>");
            }
        });
        colSemester
                .setHeader(COModuleRecommenderResources.TEXT.semesterShort());
        colSemester.setToolTip(SafeHtmlUtils
                .fromString(COModuleRecommenderResources.TEXT.semester()));

        // column for recommended icon (generated by others students)
        colRecommended.setFixed(true);
        colRecommended.setGroupable(false);
        colRecommended.setWidth(30);
        colRecommended.setCell(new RecommendedChartCell());

        // column for recommend icon (by lecturer recommendations)
        colRecommendedByLecturer.setFixed(true);
        colRecommendedByLecturer.setGroupable(false);
        colRecommendedByLecturer.setWidth(30);
        colRecommendedByLecturer.setCell(new RecommendedByLecturerChartCell());

        // column for edit recommendation icon
        colEditRecommendedByLecturer.setFixed(true);
        colEditRecommendedByLecturer.setGroupable(false);
        colEditRecommendedByLecturer.setWidth(40);
        recImage.setTitle(COModuleRecommenderResources.TEXT
                .lecturerRecommendationExists());
        infoImageAddRecommendation.setTitle(COModuleRecommenderResources.TEXT.addRecommendation());
        infoImageEditRecommendation.setTitle(COModuleRecommenderResources.TEXT.editRecommendation());
        infoImageViewRecommendation.setTitle(COModuleRecommenderResources.TEXT.viewRecommendation());
        btEditRecommendation = new TextButtonCell(new ButtonCellCustomAppearance<String>()) {
            public void render(Context context, String value, SafeHtmlBuilder sb) {
                // server sends string if recommendations exist
                this.setText(" ");
                if (user.isStaff()) {
                    int row = context.getIndex();
                    if (getStore().get(row).isLecturersModule()) {
                        
                        String toolTip = !value.equals("") ? COModuleRecommenderResources.TEXT.editRecommendation() : COModuleRecommenderResources.TEXT.addRecommendation();
                        //sb.appendHtmlConstant("<div qtitle='' qtip='" + toolTip +"'>");
                        this.setText(toolTip);
                        super.render(context, value, sb);
                       // sb.appendHtmlConstant("</div>");
                        this.setIcon(!value.equals("") ? recEditIcon : recAddIcon);
                    } else {
                        if (!value.equals("")) {
                            this.setIcon(recViewIcon);
                            //sb.appendHtmlConstant("<div qtitle='' qtip='" + COModuleRecommenderResources.TEXT.viewRecommendation() + "'>");
                            this.setText(COModuleRecommenderResources.TEXT.viewRecommendation());
                            super.render(context, value, sb);
                            //sb.appendHtmlConstant("</div>");
                        } else {
                            sb.appendHtmlConstant("");
                        }

                    }
                } else {
                    if (!value.equals("")) {
                        this.setIcon(recViewIcon);
                        //sb.appendHtmlConstant("<div qtitle='' qtip='" + COModuleRecommenderResources.TEXT.viewRecommendation() + "'>");
                        this.setText(COModuleRecommenderResources.TEXT.viewRecommendation());
                        
                        super.render(context, value, sb);
                        
                        //sb.appendHtmlConstant("</div>");
                    } else {
                        sb.appendHtmlConstant("");
                    }
                }

                this.setText(" ");

            }
        };
        // click on edit recommendation handler
        btEditRecommendation.addSelectHandler(new SelectHandler() {
            public void onSelect(SelectEvent event) {
                if (getUiHandlers() != null) {
                    getUiHandlers().onEditRecommendation(event);
                }
            }
        });
        colEditRecommendedByLecturer.setCell(btEditRecommendation);


        // column list (default columns added)
        columnList.add(colIcon);
        columnList.add(colModuleId);
        columnList.add(colLinkCO);
        columnList.add(colName);
        columnList.add(colSuperNodeName);
        columnList.add(colEditRecommendedByLecturer);

        // grid config (column model, grouping view etc)
        cm = new ColumnModel<CurriculumNodeProxy>(columnList);
        groupingView = new GroupingView<CurriculumNodeProxy>();
        groupingView.groupBy(colSuperNodeName);
        groupingView.setShowGroupedColumn(false);
        grid = new Grid<CurriculumNodeProxy>(store, cm, groupingView);
        new QuickTip(grid);
        // draggable added to modules if current user is staff
        if (user.isStaff()) {
            new GridDragSource<CurriculumNodeProxy>(grid);
        }
        grid.setLazyRowRender(3);

        // filters on columns
        GridFilters<CurriculumNodeProxy> filters = new GridFilters<CurriculumNodeProxy>();
        NumericFilter<CurriculumNodeProxy, Double> semesterFilter = new NumericFilter<CurriculumNodeProxy, Double>(props.semester(), new DoublePropertyEditor());
        NumericFilter<CurriculumNodeProxy, Double> attendanceFilter = new NumericFilter<CurriculumNodeProxy, Double>(props.attendance(), new DoublePropertyEditor());
        NumericFilter<CurriculumNodeProxy, Double> attendanceInactiveFilter = new NumericFilter<CurriculumNodeProxy, Double>(props.attendanceInactive(), new DoublePropertyEditor());
        filters.initPlugin(grid);
        filters.setLocal(true);
        filters.addFilter(new StringFilter<CurriculumNodeProxy>(props.name()));
        filters.addFilter(new StringFilter<CurriculumNodeProxy>(props
                .nodeShortName()));
        filters.addFilter(new StringFilter<CurriculumNodeProxy>(props.semesterTypeId()));
        filters.addFilter(semesterFilter);
        filters.addFilter(attendanceFilter);
        filters.addFilter(attendanceInactiveFilter);


        // uibinder
        widget = binder.createAndBindUi(this);

        // icons for buttons in toolbar
        btAttendance.setIcon(COModuleRecommenderResources.IMAGES
                .stat_16());
        btAttendance.setToolTip(COModuleRecommenderResources.TEXT.attendanceInfo());
        btAttendanceInactive.setIcon(COModuleRecommenderResources.IMAGES
                .stat_mirror_16());
        btAttendanceInactive.setToolTip(COModuleRecommenderResources.TEXT.attendanceInactiveInfo());
        btSemesterType.setIcon(COModuleRecommenderResources.IMAGES
                .folder_16());
        btSemesterType.setToolTip(COModuleRecommenderResources.TEXT.recommendedSemsterLong(COPageResources.TEXT.customTitle()));
        btSemester.setIcon(COModuleRecommenderResources.IMAGES.folder_mirror_16());
        btSemester.setToolTip(COModuleRecommenderResources.TEXT.semesterLong());
        btRecommended.setIcon(COModuleRecommenderResources.IMAGES.group());
        btRecommended.setToolTip(COModuleRecommenderResources.TEXT
                .studentRecommendationText());
        btRecommendedByLecturer.setIcon(COModuleRecommenderResources.IMAGES
                .star_16());
        btRecommendedByLecturer.setToolTip(COModuleRecommenderResources.TEXT.recommendedByLecturerLong());
        btAttendedModules.setIcon(COModuleRecommenderResources.IMAGES.blackboard_16());
        btAttendedModules.setToolTip(COModuleRecommenderResources.TEXT.alreadyAttended());

        pnList.add(grid);
        grid.getView().setForceFit(true);
        grid.getView().setAutoFill(true);
    }

    /**
     * @return view as widget
     */
    public Widget asWidget() {
        return widget;
    }

    /**
     * @return the tree
     */
    public Grid<CurriculumNodeProxy> getGrid() {
        return grid;
    }

    /**
     * @return
     */
    public ColumnConfig<CurriculumNodeProxy, Double> getColAttendanceActive() {
        return colAttendanceActive;
    }

    /**
     * ui button click handler
     */
    @UiHandler("btAttendance")
    void btAttendance(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onAttendanceSelected(event);
        }
    }

    /**
     * @return btAttendance
     */
    public ToggleButton getBtAttendance() {
        return btAttendance;
    }

    /**
     * ui button click handler
     */
    @UiHandler("btAttendanceInactive")
    void btAttendanceInactive(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onAttendanceInactiveSelected(event);
        }
    }

    /**
     * @return btAttendanceInactive
     */
    public ToggleButton getBtAttendanceInactive() {
        return btAttendanceInactive;
    }

    /**
     * ui button click handler
     */
    @UiHandler("btSemesterType")
    void onShowSemesterType(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onShowSemesterType(event);
        }
    }

    /**
     * @return btSemesterType
     */
    public ToggleButton getBtSemesterType() {
        return btSemesterType;
    }

    /**
     * @return the props
     */
    public CurriculumNodeProxySimpleProperties getProps() {
        return props;
    }

    /**
     * @return the coAttendanceInactive
     */
    public ColumnConfig<CurriculumNodeProxy, Double> getColAttendanceInactive() {
        return colAttendanceInactive;
    }

    /**
     * @return the columnList
     */
    public List<ColumnConfig<CurriculumNodeProxy, ?>> getColumnList() {
        return columnList;
    }

    /**
     * @return the colSemesterTypeRefId
     */
    public ColumnConfig<CurriculumNodeProxy, String> getColSemesterTypeRefId() {
        return colSemesterTypeRefId;
    }

    /**
     * @return the groupingView
     */
    public GroupingView<CurriculumNodeProxy> getGroupingView() {
        return groupingView;
    }

    /**
     * @return the btRecommended
     */
    public TextButton getBtRecommended() {
        return btRecommended;
    }

    /**
     * ui button click handler
     */
    @UiHandler("btRecommended")
    void onShowRecommended(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onShowRecommended(event);
        }
    }

    /**
     * @return the btRecommendedByLecturer
     */
    public TextButton getBtRecommendedByLecturer() {
        return btRecommendedByLecturer;
    }

    /**
     * @return the btRecommendedByLecturer
     */
    public TextButton getBtAttendedModules() {
        return btAttendedModules;
    }

    /**
     * ui button click handler
     */
    @UiHandler("btRecommendedByLecturer")
    void onShowRecommendedByLecturer(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onShowRecommendedByLecturer(event);
        }
    }

    /**
     * @return the colRecommendedByLecturer
     */
    public ColumnConfig<CurriculumNodeProxy, Boolean> getColRecommendedByLecturer() {
        return colRecommendedByLecturer;
    }

    /**
     * @return the colRecommended
     */
    public ColumnConfig<CurriculumNodeProxy, Boolean> getColRecommended() {
        return colRecommended;
    }

    /**
     * @return the btSemester
     */
    public ToggleButton getBtSemester() {
        return btSemester;
    }

    /**
     * ui button click handler
     */
    @UiHandler("btSemester")
    void onShowSemester(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onShowSemester(event);
        }
    }

    /**
     * ui button click handler
     */
    @UiHandler("btAttendedModules")
    void onAttendedModules(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onAttendedModules(event);
        }
    }

    /**
     * @return the colSemester
     */
    public ColumnConfig<CurriculumNodeProxy, Double> getColSemester() {
        return colSemester;
    }

    /**
     * @return the colEditRecommendedByLecturer
     */
    public ColumnConfig<CurriculumNodeProxy, String> getColEditRecommendedByLecturer() {
        return colEditRecommendedByLecturer;
    }

    /**
     * @return the colSuperNodeName
     */
    public ColumnConfig<CurriculumNodeProxy, String> getColSuperNodeName() {
        return colSuperNodeName;
    }

    /**
     * @return the user
     */
    public COClientUserModel getUser() {
        return user;
    }

    /**
     * @param user to set
     */
    public void setUser(COClientUserModel user) {
        this.user = user;
    }
}
