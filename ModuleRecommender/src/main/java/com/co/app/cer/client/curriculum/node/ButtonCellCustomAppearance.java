/**
 * Sencha GXT 3.0.1 - Sencha for GWT
 * Copyright(c) 2007-2012, Sencha, Inc.
 * licensing@sencha.com
 *
 * http://www.sencha.com/products/gxt/license/
 */
package com.co.app.cer.client.curriculum.node;

import com.sencha.gxt.theme.base.client.button.ButtonCellDefaultAppearance;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safecss.shared.SafeStyles;
import com.google.gwt.safecss.shared.SafeStylesBuilder;
import com.google.gwt.safecss.shared.SafeStylesUtils;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.cell.core.client.ButtonCell.ButtonScale;
import com.sencha.gxt.cell.core.client.ButtonCell.IconAlign;
import com.sencha.gxt.cell.core.client.SplitButtonCell;
import com.sencha.gxt.core.client.GXT;
import com.sencha.gxt.core.client.resources.CommonStyles;
import com.sencha.gxt.core.client.util.TextMetrics;
import com.sencha.gxt.theme.base.client.frame.Frame;

/**
 * Default implementation of the <code>ButtonCellAppearance</code>.
 * 
 * <p />
 * Note: The Blue and Gray buttons.
 * 
 * @param <C>
 *            the button data type
 */
public class ButtonCellCustomAppearance<C> extends
		ButtonCellDefaultAppearance<C> {

	/**
	 * Creates a button cell base appearance.
	 */
	public ButtonCellCustomAppearance() {
		super();
	}

	/**
	 * Creates a button cell base appearance using the specified resources and
	 * templates.
	 * 
	 * @param resources
	 *            the button cell resources
	 * @param templates
	 *            the templates
	 * @param frame
	 *            the frame
	 */
	public ButtonCellCustomAppearance(ButtonCellResources resources,
			ButtonCellTemplates templates, Frame frame) {
		super(resources, templates, frame);

		heightOffset = frame.getFrameSize().getHeight();
	}

	private int heightOffset;

	@Override
	public void render(final ButtonCell<C> cell, Context context, C value,
			SafeHtmlBuilder sb) {
		String constantHtml = cell.getHTML();
		boolean hasConstantHtml = constantHtml != null
				&& constantHtml.length() != 0;
		boolean isBoolean = value != null && value instanceof Boolean;
		// is a boolean always a toggle button?
		String text = hasConstantHtml ? cell.getText()
				: (value != null && !isBoolean) ? SafeHtmlUtils
						.htmlEscape(value.toString()) : "";

		ImageResource icon = cell.getIcon();
		IconAlign iconAlign = cell.getIconAlign();

		String cls = style.button();
		String arrowCls = "";
		if (cell.getMenu() != null) {

			if (cell instanceof SplitButtonCell) {
				switch (cell.getArrowAlign()) {
				case RIGHT:
					arrowCls = style.split();
					break;
				case BOTTOM:
					arrowCls = style.splitBottom();
					break;
				}

			} else {
				switch (cell.getArrowAlign()) {
				case RIGHT:
					arrowCls = style.arrow();
					break;
				case BOTTOM:
					arrowCls = style.arrowBottom();
					break;
				}
			}

		}

		ButtonScale scale = cell.getScale();

		switch (scale) {
		case SMALL:
			cls += " " + style.small();
			break;

		case MEDIUM:
			cls += " " + style.medium();
			break;

		case LARGE:
			cls += " " + style.large();
			break;
		default:
			break;
		}

		SafeStylesBuilder stylesBuilder = new SafeStylesBuilder();

		int width = -1;

		if (cell.getWidth() != -1) {
			int w = cell.getWidth();
			if (w < cell.getMinWidth()) {
				w = cell.getMinWidth();
			}
			stylesBuilder.appendTrustedString("width:" + w + "px;");
			cls += " " + style.hasWidth() + " x-has-width";
			width = w;
		} else {

			if (cell.getMinWidth() != -1) {
				TextMetrics.get().bind(style.text());
				int length = TextMetrics.get().getWidth(text);
				length += 6; // frames

				if (icon != null) {
					switch (iconAlign) {
					case LEFT:
					case RIGHT:
						length += icon.getWidth();
						break;
					case BOTTOM:
						break;
					case TOP:
						break;
					default:
						break;
					}
				}

				if (cell.getMinWidth() > length) {
					stylesBuilder.appendTrustedString("width:"
							+ cell.getMinWidth() + "px;");
					cls += " " + style.hasWidth() + " x-has-width";
					width = cell.getMinWidth();
				}
			}
		}

		final int height = cell.getHeight();
		if (height != -1) {
			stylesBuilder.appendTrustedString("height:" + height + "px;");
		}

		if (icon != null) {
			switch (iconAlign) {
			case TOP:
				arrowCls += " " + style.iconTop();
				break;
			case BOTTOM:
				arrowCls += " " + style.iconBottom();
				break;
			case LEFT:
				arrowCls += " " + style.iconLeft();
				break;
			case RIGHT:
				arrowCls += " " + style.iconRight();
				break;
			}

		} else {
			arrowCls += " " + style.noIcon();
		}

		// toggle button
		if (value == Boolean.TRUE) {
			cls += " " + frame.pressedClass();
		}

		sb.append(templates.outer(cls, new SafeStylesBuilder().toSafeStyles()));

		SafeHtmlBuilder inside = new SafeHtmlBuilder();

		String innerWrap = arrowCls;
		if (GXT.isIE6() || GXT.isIE7()) {
			arrowCls += " " + CommonStyles.get().inlineBlock();
		}

		inside.appendHtmlConstant("<div class='" + innerWrap
				+ "' style='white-space: nowrap;'>");
		inside.appendHtmlConstant("<table cellpadding=0 cellspacing=0 class='"
				+ style.mainTable() + "'>");

		if (icon != null) {
			switch (iconAlign) {
			case LEFT:
				inside.appendHtmlConstant("<tr>");
				writeIcon(inside, icon, height, text);

				inside.appendHtmlConstant("</tr>");
				break;
			case RIGHT:
				inside.appendHtmlConstant("<tr>");
				writeIcon(inside, icon, height, text);
				inside.appendHtmlConstant("</tr>");
				break;
			case TOP:
				inside.appendHtmlConstant("<tr>");
				writeIcon(inside, icon, height, text);
				inside.appendHtmlConstant("</tr>");
				if (text != null) {
					inside.appendHtmlConstant("<tr>");
					// writeText(inside, text, width, height);
					inside.appendHtmlConstant("</tr>");
				}
				break;
			case BOTTOM:
				if (text != null) {
					inside.appendHtmlConstant("<tr>");
					// writeText(inside, text, width, height);
					inside.appendHtmlConstant("</tr>");
				}
				inside.appendHtmlConstant("<tr>");
				writeIcon(inside, icon, height, text);
				inside.appendHtmlConstant("</tr>");
				break;
			}

		} else {
			inside.appendHtmlConstant("<tr>");
			if (text != null) {
				// writeText(inside, text, width, height);
			}
			inside.appendHtmlConstant("</tr>");
		}
		inside.appendHtmlConstant("</table>");

		inside.appendHtmlConstant("</div>");
		frame.render(sb, new Frame.FrameOptions(0, CommonStyles.get()
				.noFocusOutline(), stylesBuilder.toSafeStyles()), inside
				.toSafeHtml());
		sb.appendHtmlConstant("</div>");

	}

	private void writeIcon(SafeHtmlBuilder builder, ImageResource icon,
			int height, String tooltipText) {
		Image infoImage = AbstractImagePrototype.create(icon).createImage();
		if (tooltipText != null) {
			infoImage.setTitle(tooltipText);
		}
		SafeHtml iconHtml = SafeHtmlUtils.fromTrustedString(infoImage
				.toString()); // AbstractImagePrototype.create(icon).getSafeHtml();

		if (height == -1) {
			builder.append(templates.icon(style.iconWrap(), iconHtml));
		} else {
			int adjustedHeight = height - heightOffset;
			SafeStyles heightStyle = SafeStylesUtils
					.fromTrustedString("height:" + adjustedHeight + "px;");
			builder.append(templates.iconWithStyles(style.iconWrap(),
					heightStyle, iconHtml));
		}
	}

}
