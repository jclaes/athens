package com.co.app.modrec.client.ui.widgets.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionStudentsStatisticProxy;
import com.co.app.modrec.shared.requestfactory.CurriculumRequestFactory;
import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridView;
import java.util.ArrayList;
import java.util.List;

/**
 * Class widget for Curriculum students statistics
 * 
 * @author Lucas Reeh
 */
public class CurriculumStudentsStatistics extends Composite {

	Long curriculumVersionIdLoaded = null;

	private static CurriculumStudentsStatisticsUiBinder uiBinder = GWT
			.create(CurriculumStudentsStatisticsUiBinder.class);

	interface CurriculumStudentsStatisticsUiBinder extends
			UiBinder<Widget, CurriculumStudentsStatistics> {
	}

	private static final CurriculumVersionStudentsStatisticsProperties props = GWT
			.create(CurriculumVersionStudentsStatisticsProperties.class);
	@UiField
	ContentPanel gridPanel;
	ColumnModel<CurriculumVersionStudentsStatisticProxy> cm;
	ListStore<CurriculumVersionStudentsStatisticProxy> store;
	Grid<CurriculumVersionStudentsStatisticProxy> grid;
	GridView<CurriculumVersionStudentsStatisticProxy> view;
	final EventBus eventBus;
	CurriculumRequestFactory factory;

	@Inject
	public CurriculumStudentsStatistics(CurriculumRequestFactory factory,
			final EventBus eventBus) {

		// local vars
		this.eventBus = eventBus;
		this.factory = factory;
		initWidget(uiBinder.createAndBindUi(this));

		ColumnConfig<CurriculumVersionStudentsStatisticProxy, Integer> colNumberOfStudents = new ColumnConfig<CurriculumVersionStudentsStatisticProxy, Integer>(
				props.numberOfStudents(), 100,
				COModuleRecommenderResources.TEXT.statisticsNumberOfStudents());
		ColumnConfig<CurriculumVersionStudentsStatisticProxy, String> colStudyStatus = new ColumnConfig<CurriculumVersionStudentsStatisticProxy, String>(
				props.studyStatusType(), 250,
				COModuleRecommenderResources.TEXT.statisticsStudyStatus());

		List<ColumnConfig<CurriculumVersionStudentsStatisticProxy, ?>> l = new ArrayList<ColumnConfig<CurriculumVersionStudentsStatisticProxy, ?>>();
		l.add(colStudyStatus);
		l.add(colNumberOfStudents);

		cm = new ColumnModel<CurriculumVersionStudentsStatisticProxy>(l);
		store = new ListStore<CurriculumVersionStudentsStatisticProxy>(
				new ModelKeyProvider<CurriculumVersionStudentsStatisticProxy>() {
					@Override
					public String getKey(
							CurriculumVersionStudentsStatisticProxy item) {
						return item.getStudyStatusType() + "_"
								+ item.getCurriculumVersionId();
					}
				});
		view = new GridView<CurriculumVersionStudentsStatisticProxy>();
		grid = new Grid<CurriculumVersionStudentsStatisticProxy>(store, cm,
				view);
		grid.setLoadMask(true);
		view.setForceFit(true);
		view.setAutoExpandColumn(colStudyStatus);
		gridPanel.add(grid);

	}

	/**
	 * set new CurriculumVersionId and load new data from server
	 * 
	 * @param curriculumVersionId
	 */
	public void setCurriculumVersionId(Long curriculumVersionId) {
		if (!curriculumVersionId.equals(curriculumVersionIdLoaded)) {
			factory.curriculumVersionServiceRequest()
					.getStudentStatistics(curriculumVersionId)
					.fire(new Receiver<List<CurriculumVersionStudentsStatisticProxy>>() {

						@Override
						public void onSuccess(
								List<CurriculumVersionStudentsStatisticProxy> response) {
							addArtificialValueToStore(response);
						}

					});
		}
		curriculumVersionIdLoaded = curriculumVersionId;

	}

	/**
	 * add artifical entries in user friendly summarizing values to list
	 */
	private void addArtificialValueToStore(
			List<CurriculumVersionStudentsStatisticProxy> responseFromServer) {
		store.clear();
		CurriculumVersionStudentsStatisticProxy sumActive = factory.curriculumVersionServiceRequest().create(CurriculumVersionStudentsStatisticProxy.class);
		sumActive.setCurriculumVersionId(new Long(1));
		sumActive
				.setNumberOfStudents(getNumberOfStudentsActive(responseFromServer));
		sumActive.setStudyStatusType(COModuleRecommenderResources.TEXT
				.numberOfStudentsOfTypeAllActive());
		store.add(sumActive);

		CurriculumVersionStudentsStatisticProxy sumFirstSemester = factory.curriculumVersionServiceRequest().create(CurriculumVersionStudentsStatisticProxy.class);
		sumFirstSemester.setCurriculumVersionId(new Long(1));
		sumFirstSemester
				.setNumberOfStudents(getNumberOfFirstSmester(responseFromServer));
		sumFirstSemester.setStudyStatusType("  - "
				+ COModuleRecommenderResources.TEXT
						.numberOfStudentsOfTypeFirstSemester());
		store.add(sumFirstSemester);

		CurriculumVersionStudentsStatisticProxy sumSuspended = factory.curriculumVersionServiceRequest().create(CurriculumVersionStudentsStatisticProxy.class);
		sumSuspended.setCurriculumVersionId(new Long(1));
		sumSuspended
				.setNumberOfStudents(getNumberOfSuspended(responseFromServer));
		sumSuspended.setStudyStatusType("  - "
				+ COModuleRecommenderResources.TEXT
						.numberOfStudentsOfTypeVacation());
		store.add(sumSuspended);

		CurriculumVersionStudentsStatisticProxy sumFormer = factory.curriculumVersionServiceRequest().create(CurriculumVersionStudentsStatisticProxy.class);
		sumFormer.setCurriculumVersionId(new Long(1));
		sumFormer.setNumberOfStudents(getNumberOfFormer(responseFromServer));
		sumFormer.setStudyStatusType(COModuleRecommenderResources.TEXT
				.numberOfStudentsOfTypeAllInactive());
		store.add(sumFormer);
	}

	/**
	 * helper function for summarizing active students number
	 */
	private Integer getNumberOfStudentsActive(
			List<CurriculumVersionStudentsStatisticProxy> allStudentsWithStatus) {
		Integer sum = new Integer(0);
		for (CurriculumVersionStudentsStatisticProxy item : allStudentsWithStatus) {
			if (!item.getStudyStatusType().toLowerCase().equals("x")
					&& !item.getStudyStatusType().toLowerCase().equals("y")
					&& !item.getStudyStatusType().toLowerCase().equals("z")
					&& !item.getStudyStatusType().toLowerCase().equals("g")) {
				sum += item.getNumberOfStudents();
			}
		}
		return sum;
	}

	/**
	 * helper function for number of first semester students
	 */
	private Integer getNumberOfFirstSmester(
			List<CurriculumVersionStudentsStatisticProxy> allStudentsWithStatus) {
		Integer sum = new Integer(0);
		for (CurriculumVersionStudentsStatisticProxy item : allStudentsWithStatus) {
			if (item.getStudyStatusType().toLowerCase().equals("e")
					|| item.getStudyStatusType().toLowerCase().equals("b")) {
				sum += item.getNumberOfStudents();
			}
		}
		return sum;
	}

	/**
	 * helper function for number of suspended students
	 */
	private Integer getNumberOfSuspended(
			List<CurriculumVersionStudentsStatisticProxy> allStudentsWithStatus) {
		Integer sum = new Integer(0);
		for (CurriculumVersionStudentsStatisticProxy item : allStudentsWithStatus) {
			if (item.getStudyStatusType().toLowerCase().equals("u")) {
				sum += item.getNumberOfStudents();
			}
		}
		return sum;
	}

	/**
	 * helper function for number of former students
	 */
	private Integer getNumberOfFormer(
			List<CurriculumVersionStudentsStatisticProxy> allStudentsWithStatus) {
		Integer sum = new Integer(0);
		for (CurriculumVersionStudentsStatisticProxy item : allStudentsWithStatus) {
			if (item.getStudyStatusType().toLowerCase().equals("x")
					|| item.getStudyStatusType().toLowerCase().equals("y")) {
				sum += item.getNumberOfStudents();
			}
		}
		return sum;
	}
}
