/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.app.modrec.client.ui.widgets.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendancePercentageStatisticProxy;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

/**
 * interface for accessing properties of class for grid
 * 
 * @author Lucas Reeh <lreeh@tugraz.at>
 */
public interface CurriculumVersionAttendancePercentageProperties extends
		PropertyAccess<CurriculumVersionModulesAttendancePercentageStatisticProxy> {

    ModelKeyProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy> curriculumVersionId();
    
    ValueProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy, Integer> numberOfModules();

    ValueProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy, Double> offset();

    ValueProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy, Double> limit();
    
}
