package com.co.app.modrec.shared.proxy;

import com.co.app.modrec.domain.statistics.CurriculumVersionModulesAttendancePercentageStatistic;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 * Proxy Class for CurriculumVersionModulesAttendancePercentageStatistic
 *
 * @author Lucas Reeh
 *
 */
@ProxyFor(CurriculumVersionModulesAttendancePercentageStatistic.class)
public interface CurriculumVersionModulesAttendancePercentageStatisticProxy extends ValueProxy {

    abstract Long getCurriculumVersionId();

    abstract Integer getNumberOfModules();

    abstract Double getOffset();

    abstract Double getLimit();
}
