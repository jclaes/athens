package com.co.app.modrec.shared.proxy;

import com.co.app.modrec.domain.statistics.CurriculumVersionStudentsStatistic;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 * Proxy Class for CurriculumVersionStudentsStatistic
 *
 * @author Lucas Reeh
 *
 */
@ProxyFor(CurriculumVersionStudentsStatistic.class)
public interface CurriculumVersionStudentsStatisticProxy extends ValueProxy {
    
    Long getCurriculumVersionId();

    Integer getNumberOfStudents();

    String getStudyStatusType();
    
    void  setCurriculumVersionId(Long idToSet);

    void setNumberOfStudents(Integer numberOfStudentsToSet);

    void  setStudyStatusType(String statusTypeToSet);
}
