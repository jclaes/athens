package com.co.app.modrec.client.ui.widgets;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;

/**
 * View for Presenter of Modules
 *
 * @author Lucas Reeh
 *
 */
public class CurriculumVersionStatisticView extends ViewImpl implements CurriculumVersionStatisticPresenter.MyView {

    private final Widget widget;

    public interface Binder extends UiBinder<Widget, CurriculumVersionStatisticView> {
    }
    @UiField
    SimplePanel cpStudents;
    @UiField
    SimplePanel cpModules;
    @UiField
    SimplePanel cpModulesDetails;

    @Inject
    public CurriculumVersionStatisticView(final Binder binder) {
        widget = binder.createAndBindUi(this);
    }

    @Override
    public Widget asWidget() {
        return widget;
    }

    /**
     * set content in slot depending on given slot
     */
    @Override
    public void setInSlot(Object slot, Widget content) {
        if (slot == CurriculumVersionStatisticPresenter.SLOT_studentsStatistics) {
            getCpStudents().clear();
            if (content != null) {
                getCpStudents().add(content);
            }
        } else if (slot == CurriculumVersionStatisticPresenter.SLOT_moduleStatistics) {
            getCpModules().clear();
            if (content != null) {
                getCpModules().add(content);
            }
        } else if (slot == CurriculumVersionStatisticPresenter.SLOT_moduleDetailStatistics) {
            getCpModulesDetails().clear();
            if (content != null) {
                getCpModulesDetails().add(content);
            }
        } else {
            super.setInSlot(slot, content);
        }
    }

    public SimplePanel getCpStudents() {
        return cpStudents;
    }

    public SimplePanel getCpModules() {
        return cpModules;
    }

    public SimplePanel getCpModulesDetails() {
        return cpModulesDetails;
    }
}
