package com.co.app.modrec.shared.proxy;

import com.co.app.modrec.domain.CurriculumNode;
import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;

/**
 * Proxy Class for CurriculumNode
 * 
 * @author Lucas Reeh
 *
 */
@ProxyFor(CurriculumNode.class)
public interface CurriculumNodeProxy extends EntityProxy {

	/**
	 * @return the Id
	 */
	public abstract Long getId();

	/**
	 * @return the name
	 */
	public abstract String getName();

    /**
     * @param nameToSet the nameToSet to set
     */
    public abstract void setName(String nameToSet);

	/**
	 * @return the nodeType
	 */
	public abstract CurriculumNodeTypeProxy getType();
	
	/**
	 * @return the semesterType
	 */
	public abstract SemesterTypeProxy getSemesterType();

    /**
     * @param typeToSet the typeToSet to set
     */
	public abstract void setSemesterType(SemesterTypeProxy typeToSet);
	
	/**
	 * @return the superNodeId
	 */
	public abstract Long getSuperNodeId();
	
	/**
	 * @return the shortName
	 */
	public abstract String getNodeShortName();
	
	/**
	 * @return isLeaf
	 */
	public abstract Boolean isLeaf();
	
	/**
	 * @return superNodeName
	 */
	public abstract String getSuperNodeName();
	
	/**
	 * @return attendance
	 */
	public abstract Double getAttendance();

    /**
     * @param attendanceToSet the attendanceToSet to set
     */
	public abstract void setAttendance(Double attendanceToSet);
	
	/**
	 * @return attendance
	 */
	public abstract Double getAttendanceInactive();
	
	/**
	 * @return attendance
	 */
	public abstract void setAttendanceInactive(Double attendanceInactiveToSet);
	
	/**
	 * @param nameToSet the nameToSet to set
	 */
	public abstract void setSuperNodeName(String nameToSet);
	
	/**
	 * @return recommended
	 */
	public abstract Boolean isRecommended();
	
	/**
	 * @param recommended the recommended to set
	 */
	public abstract void setRecommended(Boolean recommended);
	
	/**
	 * @return recommended
	 */
	public abstract Boolean isRecommendedByLecturer();
	
	/**
	 * @param recommendedByLecturer the recommendedByLecturer to set
	 */
	public abstract void setRecommendedByLecturer(Boolean recommendedByLecturer);
	
	/**
	 * @return semester
	 */
	public abstract Double getSemester();
	
	/**
	 * @param semester the semester to set
	 */
	public abstract void setSemester(Double semester);
	
	/**
	 * HasRecommendations
	 * 
	 * @return HasRecommendations
	 */
	public abstract String getHasRecommendations();
	
	/**
	 * @param hasRecommendation to set
	 */
	public abstract void setHasRecommendations(String hasRecommendation);
	
	/**
	 * isLecturersModule
	 * 
	 * @return isLecturersModule
	 */
	public abstract Boolean isLecturersModule();
	
	/**
	 * @param lecturersModule to set
	 */
	public abstract void setLecturersModule(Boolean lecturersModule);

    /**
     * @return isModulePublic
     */
    public abstract String getModulePublic();

    /**
     *
     * @param modulePublic to set
     */
    public abstract void setModulePublic(String modulePublic);
}