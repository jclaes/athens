package com.co.app.modrec.client.ui.widgets.button;

import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Module Recommender Button View
 *
 * holds widget for Module Recommender button and search box
 *
 * @author Lucas Reeh
 *
 */
public class ModRecButtonView extends ViewWithUiHandlers<ModRecButtonUiHandlers> implements
        ModRecButtonPresenter.MyView {

    private final Widget widget;

    public interface Binder extends UiBinder<Widget, ModRecButtonView> {
    }
    @UiField
    TextButton btCurriculumVersionSelection;
    @UiField
    TextButton btSearch;
    @UiField
    TextButton btStatistics;
    @UiField
    TextField tfSearchString;
    @UiField
    ImageAnchor linkHelp;

    /**
     * Class constructor
     */
    @Inject
    public ModRecButtonView(final Binder binder) {
        widget = binder.createAndBindUi(this);
        btStatistics.setIcon(COModuleRecommenderResources.IMAGES.tablet_blue_24());
        btStatistics.setIconAlign(ButtonCell.IconAlign.LEFT);
        btCurriculumVersionSelection.setText(COModuleRecommenderResources.TEXT.navElementCurrVersionSelection() + "...");
        btCurriculumVersionSelection.setIcon(COModuleRecommenderResources.IMAGES.document_open_24());
        btCurriculumVersionSelection.setIconAlign(ButtonCell.IconAlign.LEFT);
        btSearch.setIcon(COModuleRecommenderResources.IMAGES.loupe_16());
        btSearch.setIconAlign(ButtonCell.IconAlign.LEFT);
    }

    /**
     * asWidget
     */
    @Override
    public Widget asWidget() {
        return widget;
    }

    /**
     * user pressed sign in button
     */
    @UiHandler("btCurriculumVersionSelection")
    void onButtonPressed(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onButtonPressed(event);
        }
    }

    /**
     * @return the btCOMyApps
     */
    public TextButton getBtCurriculumVersionSelection() {
        return btCurriculumVersionSelection;
    }

    /**
     * @return the btSearch
     */
    public TextButton getBtSearch() {
        return btSearch;
    }
    
    /**
     * @return the btStatistics
     */
    public TextButton getBtStatistics() {
        return btStatistics;
    }

    /**
     * @return the tfSearchString
     */
    public TextField getTfSearchString() {
        return tfSearchString;
    }

    /**
     * user pressed search button
     */
    @UiHandler("btSearch")
    void onSearchButtonPressed(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onSearchButtonPressed(event);
        }
    }
    
    /**
     * user pressed statistics
     */
    @UiHandler("btStatistics")
    void onStatisticsButtonPressed(SelectEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onStatisticsButtonPressed(event);
        }
    }

    /**
     * user pressed sign in button
     */
    @UiHandler("tfSearchString")
    void onSearchBoxEnterKeyPressed(KeyDownEvent event) {
        if (getUiHandlers() != null) {
            getUiHandlers().onSearchBoxEnterKeyPressed(event);
        }
    }
    
    
}
