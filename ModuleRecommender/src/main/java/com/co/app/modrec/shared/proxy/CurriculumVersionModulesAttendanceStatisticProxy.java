package com.co.app.modrec.shared.proxy;

import com.co.app.modrec.domain.statistics.CurriculumVersionModulesAttendanceStatistic;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 * Proxy Class for CurriculumVersionModulesAttendanceStatistic
 *
 * @author Lucas Reeh
 *
 */
@ProxyFor(CurriculumVersionModulesAttendanceStatistic.class)
public interface CurriculumVersionModulesAttendanceStatisticProxy extends ValueProxy {

    abstract Long getCurriculumVersionId();

    abstract String getSubjectTypeId();

    abstract Integer getNumberOfModules();

    abstract Integer getMinAttendance();

    abstract Integer getMaxAttendance();

    abstract Integer getAvgAttendance();
}
