package com.co.app.modrec.client.ui.widgets.button;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Image;

/**
 * extends Hyperlink for image ressource
 *
 * @author Lucas Reeh
 */
public class ImageAnchor extends Anchor {

    public ImageAnchor(){
    }

    public void setResource(ImageResource imageResource){
        Image img = new Image(imageResource);
        DOM.insertBefore(getElement(), img.getElement(), DOM.getFirstChild(getElement()));
    }


}
