package com.co.app.modrec.domain.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendanceStatisticProxy;

/**
 * Class holding statistics information on students in curriculum version
 * 
 * @author Lucas Reeh
 */
public class CurriculumVersionModulesAttendanceStatistic implements CurriculumVersionModulesAttendanceStatisticProxy {

    Long curriculumVersionId;

    String subjectTypeId;

    Integer numberOfModules = new Integer(0);

    Integer minAttendance = new Integer(0);

    Integer maxAttendance = new Integer(0);

    Integer avgAttendance = new Integer(0);

    public CurriculumVersionModulesAttendanceStatistic() {
    }

    public CurriculumVersionModulesAttendanceStatistic(Long curriculumVersionId, String subjectTypeId, Integer numberOfModules, Integer minAttendance, Integer maxAttendance, Integer avgAttendance) {
        this.curriculumVersionId = curriculumVersionId;
        this.subjectTypeId = subjectTypeId;
        this.numberOfModules = numberOfModules;
        this.minAttendance = minAttendance;
        this.maxAttendance = maxAttendance;
        this.avgAttendance = avgAttendance;
    }

    @Override
    public Long getCurriculumVersionId() {
        return curriculumVersionId;
    }

    public void setCurriculumVersionId(Long curriculumVersionId) {
        this.curriculumVersionId = curriculumVersionId;
    }

    @Override
    public String getSubjectTypeId() {
        return subjectTypeId;
    }

    public void setSubjectTypeId(String subjectTypeId) {
        this.subjectTypeId = subjectTypeId;
    }

    @Override
    public Integer getNumberOfModules() {
        return numberOfModules;
    }

    public void setNumberOfModules(Integer numberOfModules) {
        this.numberOfModules = numberOfModules;
    }

    @Override
    public Integer getMinAttendance() {
        return minAttendance;
    }

    public void setMinAttendance(Integer minAttendance) {
        this.minAttendance = minAttendance;
    }

    @Override
    public Integer getMaxAttendance() {
        return maxAttendance;
    }

    public void setMaxAttendance(Integer maxAttendance) {
        this.maxAttendance = maxAttendance;
    }

    @Override
    public Integer getAvgAttendance() {
        return avgAttendance;
    }

    public void setAvgAttendance(Integer avgAttendance) {
        this.avgAttendance = avgAttendance;
    }
}
