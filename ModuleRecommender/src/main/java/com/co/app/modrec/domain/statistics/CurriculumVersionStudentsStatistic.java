package com.co.app.modrec.domain.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionStudentsStatisticProxy;

/**
 * Class holding statistics information on students in curriculum version
 *
 * @author Lucas Reeh
 */
public class CurriculumVersionStudentsStatistic implements CurriculumVersionStudentsStatisticProxy {

    Long curriculumVersionId;

    String studyStatusType;

    Integer numberOfStudents = new Integer(0);

    public CurriculumVersionStudentsStatistic() {
    }

    public CurriculumVersionStudentsStatistic(Long curriculumVersionId, String studyStatusType, Integer numberOfStudents) {
        this.curriculumVersionId = curriculumVersionId;
        this.studyStatusType = studyStatusType;
        this.numberOfStudents = numberOfStudents;
    }

    @Override
    public Long getCurriculumVersionId() {
        return curriculumVersionId;
    }

    public void setCurriculumVersionId(Long curriculumVersionId) {
        this.curriculumVersionId = curriculumVersionId;
    }

    @Override
    public String getStudyStatusType() {
        return studyStatusType;
    }

    public void setStudyStatusType(String studyStatusType) {
        this.studyStatusType = studyStatusType;
    }

    @Override
    public Integer getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(Integer numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }
}
