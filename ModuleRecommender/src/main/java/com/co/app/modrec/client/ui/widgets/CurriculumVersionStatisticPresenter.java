package com.co.app.modrec.client.ui.widgets;

import com.co.app.modrec.client.ModRecHomePresenter;
import com.co.app.modrec.client.ui.widgets.statistics.CurriculumAttendancePercentageStatistics;
import com.co.app.modrec.client.ui.widgets.statistics.CurriculumAttendanceStatistics;
import com.co.app.modrec.client.ui.widgets.statistics.CurriculumStudentsStatistics;
import com.co.app.modrec.shared.requestfactory.CurriculumRequestFactory;
import com.co.client.place.PageNameTokens;
import com.co.core.auth.client.COClientUserHolder;
import com.co.core.auth.client.event.COClientUserChangedEvent;
import com.co.core.auth.shared.COClientUserModel;
import com.co.core.shared.resources.COResources;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;

/**
 * Presenter of Modules tree and grid with toolbar
 *
 * @author Lucas Reeh
 *
 */
public class CurriculumVersionStatisticPresenter extends Presenter<CurriculumVersionStatisticPresenter.MyView, CurriculumVersionStatisticPresenter.MyProxy> {

    String pCurriculumVersionId = null;

    public interface MyView extends View {

        public SimplePanel getCpModulesDetails();

        public SimplePanel getCpModules();

        public SimplePanel getCpStudents();
    }
    
    public static final Object SLOT_studentsStatistics = new Object();
    
    public static final Object SLOT_moduleStatistics = new Object();
    
    public static final Object SLOT_moduleDetailStatistics = new Object();
    
    @Inject
    CurriculumRequestFactory factory;
    
    @Inject
    CurriculumStudentsStatistics studentsStatisticsWidget;
    
    // TODO
    @Inject
    CurriculumAttendanceStatistics modulesStatisticsWidget;
    
    // TODO
    @Inject
    CurriculumAttendancePercentageStatistics moduleDetailsStatisticsWidget;

    COClientUserModel user;
    
    PlaceManager placeManager;
    
    /**
	 * handles {@link COClientUserChangedEvent}
	 */
	public final COClientUserChangedEvent.COClientUserChangedHandler coClientUserChangedHandler = new COClientUserChangedEvent.COClientUserChangedHandler() {

		/**
		 * sets header content in right content slot
		 */
		public void onCOClientUserChanged(COClientUserChangedEvent event) {
			user = event.getCoClientUser();
			setWidgetsInSlots();
		}

	};
    
    @ProxyCodeSplit
    @NameToken(PageNameTokens.statistics)
    public interface MyProxy extends ProxyPlace<CurriculumVersionStatisticPresenter> {
    }

    /**
     * class constructor
     */
    @Inject
    public CurriculumVersionStatisticPresenter(final EventBus eventBus, final MyView view,
            final MyProxy proxy, COClientUserHolder userHolder, PlaceManager placeManager) {
        super(eventBus, view, proxy);
        this.user = userHolder.getUser();
        this.placeManager = placeManager;
    }

    /**
     * revealInParent
     */
    @Override
    protected void revealInParent() {
        RevealContentEvent.fire(this, ModRecHomePresenter.SLOT_mainContent, this);
    }

    /**
     * prepareFromRequest
     */
    @Override
    public void prepareFromRequest(PlaceRequest request) {
        super.prepareFromRequest(request);
        pCurriculumVersionId = request.getParameter("pCurriculumVersionId", "0");
    }

    /**
     * onBind
     */
    @Override
    protected void onBind() {
        super.onBind();
        registerHandler(getEventBus().addHandler(COClientUserChangedEvent.getType(), coClientUserChangedHandler));
    }
    
    /**
     * onReveal
     */
    @Override
    protected void onReveal() {
        super.onReveal();
        factory.initialize(getEventBus());
        setWidgetsInSlots();
        checkIfIsStaff();
    }
    
    /**
     * onReset
     */
    @Override
    protected void onReset() {
        setWidgetsInSlots();
    }
    
    /**
     * sets widgets in corresponding slot
     */
    private void setWidgetsInSlots() {
        if (pCurriculumVersionId == null) {
            AlertMessageBox alert = new AlertMessageBox(
                    COResources.TEXT_ERROR.error(), "Parameter: pCurriculumVersionId must not be null");
            alert.show();
        } else if (pCurriculumVersionId.equals("0")) {
            AlertMessageBox alert = new AlertMessageBox(
                    COResources.TEXT_ERROR.error(), "Parameter: pCurriculumVersionId must not be null");
            alert.show();
        } else {
        	
        	if (this.user.isStaff()) {
	        	getView().setInSlot(SLOT_studentsStatistics, studentsStatisticsWidget);
	            getView().setInSlot(SLOT_moduleStatistics, modulesStatisticsWidget);
	            getView().setInSlot(SLOT_moduleDetailStatistics, moduleDetailsStatisticsWidget);
	            setNewCurriculumVersionId(new Long(pCurriculumVersionId));
        	}
        }
    }
    
    /**
     * calls setters on widgets for loading new data for curriculumVersionId
     * 
     * @param curriculumVersionId 
     */
    private void setNewCurriculumVersionId(Long curriculumVersionId) {
        studentsStatisticsWidget.setCurriculumVersionId(curriculumVersionId);
        modulesStatisticsWidget.setCurriculumVersionId(curriculumVersionId);
        moduleDetailsStatisticsWidget.setCurriculumVersionId(curriculumVersionId);
    }
    
    /**
     * check current user
     */
    private void checkIfIsStaff() {
        if (this.user == null) {
            PlaceRequest request = new PlaceRequest(PageNameTokens.getNotauthorized());
            placeManager.revealPlace(request);
        }
        if (!this.user.isStaff()) {
            PlaceRequest request = new PlaceRequest(PageNameTokens.getNotauthorized());
            placeManager.revealPlace(request);
        }
    }
}
