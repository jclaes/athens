package com.co.app.modrec.domain.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendancePercentageStatisticProxy;

/**
 * Class holding statistics information on students in curriculum version
 *
 * @author Lucas Reeh
 */
public class CurriculumVersionModulesAttendancePercentageStatistic implements CurriculumVersionModulesAttendancePercentageStatisticProxy {

    Long curriculumVersionId;

    Integer numberOfModules = new Integer(0);

    Double offset = new Double(0);

    Double limit = new Double(0);

    public CurriculumVersionModulesAttendancePercentageStatistic() {
    }

    @Override
    public Long getCurriculumVersionId() {
        return curriculumVersionId;
    }

    public void setCurriculumVersionId(Long curriculumVersionId) {
        this.curriculumVersionId = curriculumVersionId;
    }

    @Override
    public Integer getNumberOfModules() {
        return numberOfModules;
    }

    public void setNumberOfModules(Integer numberOfModules) {
        this.numberOfModules = numberOfModules;
    }

    @Override
    public Double getOffset() {
        return offset;
    }

    public void setOffset(Double offset) {
        this.offset = offset;
    }

    @Override
    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }
}
