package com.co.app.modrec.client.ui.widgets.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendanceStatisticProxy;
import com.co.app.modrec.shared.requestfactory.CurriculumRequestFactory;
import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.Store.StoreSortInfo;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridView;
import java.util.ArrayList;
import java.util.List;

/**
 * Class widget for Curriculum students statistics
 *
 * @author Lucas Reeh
 */
public class CurriculumAttendanceStatistics extends Composite {

	Long curriculumVersionIdLoaded = null;
	
    private static CurriculumAttendanceStatisticsUiBinder uiBinder = GWT.create(CurriculumAttendanceStatisticsUiBinder.class);

    interface CurriculumAttendanceStatisticsUiBinder extends UiBinder<Widget, CurriculumAttendanceStatistics> {
    }
    private static final CurriculumVersionAttendanceStatisticsProperties props = GWT.create(CurriculumVersionAttendanceStatisticsProperties.class);

    @UiField
    ContentPanel gridPanel;
    ColumnModel<CurriculumVersionModulesAttendanceStatisticProxy> cm;
    ListStore<CurriculumVersionModulesAttendanceStatisticProxy> store;
    Grid<CurriculumVersionModulesAttendanceStatisticProxy> grid;
    GridView<CurriculumVersionModulesAttendanceStatisticProxy> view;
    ValueProvider<CurriculumVersionModulesAttendanceStatisticProxy, String> subjectTypeNameValueProvider = new ValueProvider<CurriculumVersionModulesAttendanceStatisticProxy, String>() {
        @Override
        public String getValue(CurriculumVersionModulesAttendanceStatisticProxy object) {
            if (object.getSubjectTypeId().equals("WAHL")) {
                return COModuleRecommenderResources.TEXT.subjectTypeElective();
            } else if (object.getSubjectTypeId().equals("PFLICHT")) {
                return COModuleRecommenderResources.TEXT.subjectTypeCompulsory();
            } else if (object.getSubjectTypeId().equals("WPFLICHT")) {
                return COModuleRecommenderResources.TEXT.subjectTypeCompulsoryElective();
            }
            return COModuleRecommenderResources.TEXT.subjectTypeNone();
        }

        @Override
        public void setValue(CurriculumVersionModulesAttendanceStatisticProxy object, String value) {
        }

        @Override
        public String getPath() {
            return "subject_type_name";
        }
    };
    final EventBus eventBus;
    CurriculumRequestFactory factory;

    @Inject
    public CurriculumAttendanceStatistics(CurriculumRequestFactory factory,
            final EventBus eventBus) {

        // local vars
        this.eventBus = eventBus;
        this.factory = factory;
        initWidget(uiBinder.createAndBindUi(this));

        ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, String> colSubjectType = new ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, String>(
                subjectTypeNameValueProvider, 200, COModuleRecommenderResources.TEXT.statisticsAttendanceSubjectType());
        ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer> colNumberOfModules = new ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer>(props.numberOfModules(), 100, COModuleRecommenderResources.TEXT.statisticsNumberOfModules());
        ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer> colAttMin = new ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer>(props.minAttendance(), 150, COModuleRecommenderResources.TEXT.statisticsAttendanceMinNumberOfStudents());
        ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer> colAttAvg = new ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer>(props.avgAttendance(), 150, COModuleRecommenderResources.TEXT.statisticsAttendanceAvgNumberOfStudents());
        ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer> colAttMax = new ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, Integer>(props.maxAttendance(), 150, COModuleRecommenderResources.TEXT.statisticsAttendanceMaxNumberOfStudents());

        List<ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, ?>> l = new ArrayList<ColumnConfig<CurriculumVersionModulesAttendanceStatisticProxy, ?>>();
        l.add(colSubjectType);
        l.add(colNumberOfModules);
        l.add(colAttMin);
        l.add(colAttAvg);
        l.add(colAttMax);
        cm = new ColumnModel<CurriculumVersionModulesAttendanceStatisticProxy>(l);
        store = new ListStore<CurriculumVersionModulesAttendanceStatisticProxy>(new ModelKeyProvider<CurriculumVersionModulesAttendanceStatisticProxy>() {
            @Override
            public String getKey(CurriculumVersionModulesAttendanceStatisticProxy item) {
                return item.getSubjectTypeId() + "_" + item.getCurriculumVersionId();
            }
        });
        store.addSortInfo(new StoreSortInfo<CurriculumVersionModulesAttendanceStatisticProxy>(props.subjectTypeId(), SortDir.ASC));
        view = new GridView<CurriculumVersionModulesAttendanceStatisticProxy>();
        grid = new Grid<CurriculumVersionModulesAttendanceStatisticProxy>(store, cm, view);
        grid.setLoadMask(true);
        view.setForceFit(true);
        view.setAutoExpandColumn(colSubjectType);
        gridPanel.add(grid);

    }

    /**
     * set new CurriculumVersionId and load new data from server
     *
     * @param curriculumVersionId
     */
    public void setCurriculumVersionId(Long curriculumVersionId) {
    	if (!curriculumVersionId.equals(curriculumVersionIdLoaded)) {
    		factory.curriculumVersionServiceRequest().getModuleAttendanceStatistics(curriculumVersionId).fire(new Receiver<List<CurriculumVersionModulesAttendanceStatisticProxy>>() {
                @Override
                public void onSuccess(List<CurriculumVersionModulesAttendanceStatisticProxy> response) {
                    store.replaceAll(response);
                }
            });
		}
		curriculumVersionIdLoaded = curriculumVersionId;
        
    }


    
}
