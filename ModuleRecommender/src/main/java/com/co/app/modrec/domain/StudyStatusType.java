package com.co.app.modrec.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.co.core.server.util.EMF;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * The persistent class for the semester type database table.
 *
 */
@Entity
@Table(name = "ST_STUDIUM_STATUS_TYP")
public class StudyStatusType implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * @return the serialversionuid
     */
    public long getVersion() {
        return serialVersionUID;
    }
    // primary key
    @Id
    @Column(name = "NR")
    private Long id;
    // short name for referencing
    @Column(name = "KENNUNG")
    private String refId;
    // name
    private String name;

    /**
     * class constructor
     */
    public StudyStatusType() {
    }

    /**
     * find record by id
     *
     * @param id
     * @return StudyStatusType
     */
    public static StudyStatusType findStudyStatusType(Long id) {
        if (id == null) {
            return null;
        }
        return EMF.getEm().find(StudyStatusType.class, id);
    }

    /**
     * find record by refId
     *
     * @param id
     * @return StudyStatusType
     */
    public static StudyStatusType findStudyStatusTypeByRefId(String refId) {
        if (refId == null) {
            return null;
        }
        CriteriaBuilder criteriaBuilder = EMF.getEm().getCriteriaBuilder();
        CriteriaQuery query = criteriaBuilder.createQuery();
        Root<StudyStatusType> root = query.from(StudyStatusType.class);
        ParameterExpression<String> pRefId = criteriaBuilder.parameter(String.class);
        Predicate refIdFilter = criteriaBuilder.equal(root.get("refId"), pRefId);
        query.where(refIdFilter);
        query.select(root);
        TypedQuery<StudyStatusType> q = EMF.getEm().createQuery(query);
        q.setParameter(pRefId, refId);
        return q.getSingleResult();
    }

    /*
     * default setter/getter
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRefId() {
        return this.refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}