package com.co.app.modrec.client.ui.widgets.statistics;

import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendancePercentageStatisticProxy;
import com.co.app.modrec.shared.requestfactory.CurriculumRequestFactory;
import com.co.app.modrec.shared.resources.COModuleRecommenderResources;
import com.co.core.auth.shared.COClientUserModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.sencha.gxt.chart.client.chart.Chart;
import com.sencha.gxt.chart.client.chart.axis.CategoryAxis;
import com.sencha.gxt.chart.client.chart.axis.NumericAxis;
import com.sencha.gxt.chart.client.chart.series.BarSeries;
import com.sencha.gxt.chart.client.draw.Gradient;
import com.sencha.gxt.chart.client.draw.RGB;
import com.sencha.gxt.chart.client.draw.sprite.TextSprite;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.SortDir;
import com.sencha.gxt.data.shared.Store.StoreSortInfo;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.GridView;
import java.util.ArrayList;
import java.util.List;

/**
 * Class widget for Curriculum students statistics
 *
 * @author Lucas Reeh
 */
public class CurriculumAttendancePercentageStatistics extends Composite {
	
	Long curriculumVersionIdLoaded = null;

    private static CurriculumAttendancePercentageStatisticsUiBinder uiBinder = GWT.create(CurriculumAttendancePercentageStatisticsUiBinder.class);

    interface CurriculumAttendancePercentageStatisticsUiBinder extends UiBinder<Widget, CurriculumAttendancePercentageStatistics> {
    }
    private static final CurriculumVersionAttendancePercentageProperties props = GWT.create(CurriculumVersionAttendancePercentageProperties.class);
    @UiField
    ContentPanel chartPanel;
    @UiField
    ContentPanel gridPanel;
    ColumnModel<CurriculumVersionModulesAttendancePercentageStatisticProxy> cm;
    ListStore<CurriculumVersionModulesAttendancePercentageStatisticProxy> store;
    ListStore<CurriculumVersionModulesAttendancePercentageStatisticProxy> storeFiltered;
    Grid<CurriculumVersionModulesAttendancePercentageStatisticProxy> grid;
    GridView<CurriculumVersionModulesAttendancePercentageStatisticProxy> view;
    ValueProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy, String> rangeStringValueProvider = new ValueProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy, String>() {
        @Override
        public String getValue(CurriculumVersionModulesAttendancePercentageStatisticProxy object) {
            if (object.getLimit() <= 0) {
                return "0";
            }
            return object.getOffset().toString() + "%" + " - " + object.getLimit().toString() + "%";
        }

        @Override
        public void setValue(CurriculumVersionModulesAttendancePercentageStatisticProxy object, String value) {
        }

        @Override
        public String getPath() {
            return "range_string";
        }
    };
    final EventBus eventBus;
    COClientUserModel user;
    CurriculumRequestFactory factory;
    PlaceManager placeManager;
    final Chart<CurriculumVersionModulesAttendancePercentageStatisticProxy> chartCurrVersModAttStatPerc;

    @Inject
    public CurriculumAttendancePercentageStatistics(CurriculumRequestFactory factory,
            final EventBus eventBus) {

        // local vars
        this.eventBus = eventBus;
        this.factory = factory;
        initWidget(uiBinder.createAndBindUi(this));

        ColumnConfig<CurriculumVersionModulesAttendancePercentageStatisticProxy, String> colRange = new ColumnConfig<CurriculumVersionModulesAttendancePercentageStatisticProxy, String>(
                rangeStringValueProvider, 150, COModuleRecommenderResources.TEXT.statisticsAttendancePercentageRange());
        ColumnConfig<CurriculumVersionModulesAttendancePercentageStatisticProxy, Integer> colNumberOfModules = new ColumnConfig<CurriculumVersionModulesAttendancePercentageStatisticProxy, Integer>(props.numberOfModules(), 75, COModuleRecommenderResources.TEXT.statisticsNumberOfModules());


        List<ColumnConfig<CurriculumVersionModulesAttendancePercentageStatisticProxy, ?>> l = new ArrayList<ColumnConfig<CurriculumVersionModulesAttendancePercentageStatisticProxy, ?>>();
        l.add(colRange);
        l.add(colNumberOfModules);
        cm = new ColumnModel<CurriculumVersionModulesAttendancePercentageStatisticProxy>(l);
        store = new ListStore<CurriculumVersionModulesAttendancePercentageStatisticProxy>(new ModelKeyProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy>() {
            @Override
            public String getKey(CurriculumVersionModulesAttendancePercentageStatisticProxy item) {
                return item.getLimit() + "_" + item.getCurriculumVersionId();
            }
        });
        view = new GridView<CurriculumVersionModulesAttendancePercentageStatisticProxy>();
        grid = new Grid<CurriculumVersionModulesAttendancePercentageStatisticProxy>(store, cm, view);
        grid.setLoadMask(true);
        view.setForceFit(true);
        view.setAutoExpandColumn(colRange);
        gridPanel.add(grid);
        
        
        
        storeFiltered = new ListStore<CurriculumVersionModulesAttendancePercentageStatisticProxy>(new ModelKeyProvider<CurriculumVersionModulesAttendancePercentageStatisticProxy>() {
            @Override
            public String getKey(CurriculumVersionModulesAttendancePercentageStatisticProxy item) {
                return item.getLimit() + "_" + item.getCurriculumVersionId();
            }
        });
        
        storeFiltered.addSortInfo(new StoreSortInfo<CurriculumVersionModulesAttendancePercentageStatisticProxy>(props.offset(), SortDir.DESC));
        chartCurrVersModAttStatPerc = new Chart<CurriculumVersionModulesAttendancePercentageStatisticProxy>();
        chartCurrVersModAttStatPerc.setStore(storeFiltered);
        chartCurrVersModAttStatPerc.setShadowChart(true);

        Gradient background = new Gradient("backgroundGradient", 0);
        background.addStop(0, new RGB("#ffffff"));
        background.addStop(100, new RGB("#eaf1f8"));
        chartCurrVersModAttStatPerc.addGradient(background);
        chartCurrVersModAttStatPerc.setBackground(background);

        NumericAxis<CurriculumVersionModulesAttendancePercentageStatisticProxy> axisCurrVersModAttStatPerc = new NumericAxis<CurriculumVersionModulesAttendancePercentageStatisticProxy>();
        axisCurrVersModAttStatPerc.setPosition(Chart.Position.BOTTOM);
        axisCurrVersModAttStatPerc.addField(props.numberOfModules());
        TextSprite title = new TextSprite(COModuleRecommenderResources.TEXT.statisticsNumberOfStudents());
        title.setFontSize(14);
        axisCurrVersModAttStatPerc.setTitleConfig(title);
        axisCurrVersModAttStatPerc.setDisplayGrid(true);
        axisCurrVersModAttStatPerc.setMinimum(0);
        chartCurrVersModAttStatPerc.addAxis(axisCurrVersModAttStatPerc);

        CategoryAxis<CurriculumVersionModulesAttendancePercentageStatisticProxy, String> catAxisCurrVersModAttStatPerc = new CategoryAxis<CurriculumVersionModulesAttendancePercentageStatisticProxy, String>();
        catAxisCurrVersModAttStatPerc.setPosition(Chart.Position.LEFT);
        catAxisCurrVersModAttStatPerc.setField(rangeStringValueProvider);
        title = new TextSprite(COModuleRecommenderResources.TEXT.statisticsAttendancePercentageRange());
        title.setFontSize(14);
        catAxisCurrVersModAttStatPerc.setTitleConfig(title);
        chartCurrVersModAttStatPerc.addAxis(catAxisCurrVersModAttStatPerc);

        final BarSeries<CurriculumVersionModulesAttendancePercentageStatisticProxy> barCurrVersModAttStatPerc = new BarSeries<CurriculumVersionModulesAttendancePercentageStatisticProxy>();
        barCurrVersModAttStatPerc.setYAxisPosition(Chart.Position.BOTTOM);
        barCurrVersModAttStatPerc.addYField(props.numberOfModules());
        barCurrVersModAttStatPerc.addColor(new RGB(213, 70, 121));
        barCurrVersModAttStatPerc.setHighlighting(true);
        chartCurrVersModAttStatPerc.addSeries(barCurrVersModAttStatPerc);

        chartPanel.add(chartCurrVersModAttStatPerc);
        

    }

    /**
     * set new CurriculumVersionId and load new data from server
     *
     * @param curriculumVersionId
     */
    public void setCurriculumVersionId(Long curriculumVersionId) {
    	if (!curriculumVersionId.equals(curriculumVersionIdLoaded)) {
    		factory.curriculumVersionServiceRequest().getModuleAttendancePercentageStatistics(curriculumVersionId).fire(new Receiver<List<CurriculumVersionModulesAttendancePercentageStatisticProxy>>() {
                @Override
                public void onSuccess(List<CurriculumVersionModulesAttendancePercentageStatisticProxy> response) {
                    store.replaceAll(response);
                    storeFiltered.clear();
                    for (CurriculumVersionModulesAttendancePercentageStatisticProxy item: store.getAll()) {
                    	if (!item.getLimit().equals(new Double(0))) {
                    		storeFiltered.add(item);
                    	}
                    }
                    chartCurrVersModAttStatPerc.redrawChart();
                }
            });
		}
		curriculumVersionIdLoaded = curriculumVersionId;
        
    }

    
}
