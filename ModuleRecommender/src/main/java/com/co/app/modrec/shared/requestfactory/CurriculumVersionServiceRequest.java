package com.co.app.modrec.shared.requestfactory;

import com.co.app.modrec.domain.CurriculumVersionPagingLoadResultBean;
import com.co.app.modrec.domain.CurriculumVersionService;
import com.co.app.modrec.domain.CurriculumVersionServiceLocator;
import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendancePercentageStatisticProxy;
import com.co.app.modrec.shared.proxy.CurriculumVersionModulesAttendanceStatisticProxy;
import com.co.app.modrec.shared.proxy.CurriculumVersionProxy;
import com.co.app.modrec.shared.proxy.CurriculumVersionStudentsStatisticProxy;
import com.google.web.bindery.requestfactory.shared.*;
import com.sencha.gxt.data.shared.SortInfo;
import com.sencha.gxt.data.shared.loader.FilterConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.List;

/**
 * Request Context for CurriculumVersionService
 * 
 * @author Lucas Reeh
 * 
 */
@Service(value = CurriculumVersionService.class, locator = CurriculumVersionServiceLocator.class)
public interface CurriculumVersionServiceRequest extends RequestContext {

	@ProxyFor(CurriculumVersionPagingLoadResultBean.class)
	public interface CurriculumVersionPagingLoadResultProxy extends ValueProxy,
			PagingLoadResult<CurriculumVersionProxy> {
		@Override
	    public List<CurriculumVersionProxy> getData();

	}

	Request<CurriculumVersionPagingLoadResultProxy> getCurriculumVersions(
			int offset, int limit, List<? extends SortInfo> sortInfo,
			List<? extends FilterConfig> filterConfig);

	Request<CurriculumVersionPagingLoadResultProxy> getStudentsCurriculumVersions(
			int offset, int limit, List<? extends SortInfo> sortInfo,
			List<? extends FilterConfig> filterConfig);

    Request<List<CurriculumVersionModulesAttendanceStatisticProxy>> getModuleAttendanceStatistics(Long curriculumVersionId);

    Request<List<CurriculumVersionStudentsStatisticProxy>> getStudentStatistics(Long curriculumVersionId);

    Request<List<CurriculumVersionModulesAttendancePercentageStatisticProxy>> getModuleAttendancePercentageStatistics(Long curriculumVersionId);

}
