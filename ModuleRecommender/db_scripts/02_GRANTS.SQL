/** grants for authentication procedures */
GRANT
     CREATE ANY MATERIALIZED VIEW
   , ALTER ANY MATERIALIZED VIEW
   , DROP ANY MATERIALIZED VIEW
   , QUERY REWRITE
   , GLOBAL QUERY REWRITE
   TO TUM_LEMORA
   WITH ADMIN OPTION
/
grant execute on co_sec.puAuthentify to TUM_LEMORA
/
grant execute on co_sec.inSession to TUM_LEMORA
/
grant execute on sys.dbms_session to TUM_LEMORA
/
grant execute on co_sec.puSession to TUM_LEMORA
/
grant select on TUG_NEW.IDENTITAETEN to TUM_LEMORA
/
grant select on TUG_NEW.ST_PERSONEN to TUM_LEMORA
/
/** grants for business data */
grant select on tug_new.stp_studienplaene to TUM_LEMORA
/
grant select on tug_new.stp_studien to TUM_LEMORA
/
grant select on stud.codex_abschlussziele to TUM_LEMORA
/
grant select on stud.codex_studienkennzahlen to TUM_LEMORA
/
grant select on stud.st_studien to TUM_LEMORA
/
grant select on tug_new.stp_kn_stud_ergebnisse to TUM_LEMORA
/
grant select on tug_new.stp_knoten to TUM_LEMORA
/
grant select on tug_new.stp_vorschrift_arten to TUM_LEMORA
/
grant select on tug_new.pu_stp_fa_mo_knoten_v to TUM_LEMORA
/
grant select on tug_new.pu_stp_fa_mo_knoten_v to TUM_LEMORA
/
grant select on tug_new.in_stp_st_stud_zu_ber_small_v to TUM_LEMORA
/
grant select on stud.st_studien to TUM_LEMORA
/
grant select on stud.st_semester to TUM_LEMORA
/
grant execute on tug_new.puStpKnoten to TUM_LEMORA
/
grant select on tug_new.stp_semester_typen to TUM_LEMORA
/
grant select on tug_new.stp_knoten_typen to TUM_LEMORA
/
grant select on tug_new.stp_kntyp_active_ke_v to TUM_LEMORA
/
grant select on stud.st_fachsemester_v to TUM_LEMORA
/
grant execute on  tug_new.wbStpUtl to TUM_LEMORA
/
grant all on tug_new.in_stp_st_stud_zu_ber_s_v to TUM_LEMORA
/
grant select on tug_new.pu_lehrauftraege_alle_v to TUM_LEMORA
/
grant select on tug_new.pu_stp_knoten_lv_v to TUM_LEMORA
/
grant select on pv.pv_dp_kandidaten to TUM_LEMORA
/
grant select on stud.st_studium_status to TUM_LEMORA
/
grant select on stud.st_studium_status_typ to TUM_LEMORA
/
grant execute on pv.pufinalexam to TUM_LEMORA
/
grant execute on tug_new.puStpModHB to TUM_LEMORA
/
grant select on tug_new.pu_modhb_udf_v to TUM_LEMORA
/
grant execute on stud.puabschluss to TUM_LEMORA
/
grant select on pv.pu_exam_leistung_v to TUM_LEMORA
/
grant execute on stud.puStudien to TUM_LEMORA
/
