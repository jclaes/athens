create or replace view tum_lemora.MODREC_CURR_VERS_MODULE_STAT_V as (

select m.curriculum_version_id
     , nvl(m.subject_type_ref_id, 'OTHERS') subject_type_ref_id
     , count(m.node_id) number_of_modules
     , min( att.attendance_figure ) min_attendance_figure
     , max( att.attendance_figure ) max_attendance_figure
     , round(avg( att.attendance_figure )) avg_attendance_figure
  from modrec_module_node_mv m
     , modrec_node_attendance_mv att
 where att.node_id = m.node_id
 group by m.curriculum_version_id
        , m.subject_type_ref_id

)
/