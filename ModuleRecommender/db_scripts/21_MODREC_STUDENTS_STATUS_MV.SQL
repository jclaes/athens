CREATE MATERIALIZED VIEW tum_lemora.modrec_students_status_mv
  PCTFREE     10
  MAXTRANS    255
  TABLESPACE  tugonline
  STORAGE   (
    INITIAL     65536
    NEXT        1048576
    MINEXTENTS  1
    MAXEXTENTS  2147483645
  )
LOGGING
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE START WITH to_date('17-11-2012 12:40 PM','DD-MM-YYYY HH12:MI PM') NEXT sysdate + 1
AS
(
select students.stp_stp_nr curriculum_Version_Id
     , students.nr         student_Study_Id
     , stud.puStudien.getStudiumStatusTypKb(students.nr, sysdate) study_Status
  from ( select ex.st_studium_nr    nr
              , ex.stp_stp_1_nr     stp_stp_nr
           from ex_students_mv ex
          union
         select active.st_studium_nr nr
              , active.stp_stp_1_nr  stp_stp_nr
           from active_students_mv active
       ) students
)
/
