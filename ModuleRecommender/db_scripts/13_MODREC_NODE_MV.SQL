CREATE MATERIALIZED VIEW MODREC_NODE_MV
  TABLESPACE TUGONLINE
  PARALLEL ( DEGREE 1 )
  BUILD IMMEDIATE
  REFRESH FORCE START WITH TO_DATE('17-11-2012 12:35 PM','DD-MM-YYYY HH12:MI PM')
    NEXT SYSDATE + 1
  AS
  (
select a.id
     , a.name
	 , a.nameEnglish
     , a.node_type_id
     , a.curriculum_version_id
     , a.super_node_id
     , a.node_shortname
     , a.semester_type_id
     , m.subject_type_ref_id
     , m.subject_type_name
     , coalesce( m_a.attendance_figure
        / ( select case when count(1) = 0 then 1 else count(1) end a_count
              from  active_students_mv active
             where  active.stp_stp_1_nr = a.curriculum_version_id
          ) * 100 , 0 ) attendance_figure
     , coalesce( m_ai.attendance_figure
        / ( select case when count(1) = 0 then 1 else count(1) end a_count
              from ex_students_mv inactive
             where  inactive.stp_stp_1_nr = a.curriculum_version_id
          ) * 100 , 0 ) attendance_inactive_figure
     , sem.median_semester                      semester
     , a.sortstring                             sortstring
     , m.is_public_flag                         is_mhb_public_flag
  from modrec_node_v a
     , modrec_module_node_mv m
     , modrec_node_attendance_mv m_a
     , modrec_node_att_inactive_mv m_ai
     , modrec_node_att_sem_median_mv sem
 where m.node_id(+) = a.id
   and m_a.node_id(+) = a.id
   and m_ai.node_id(+) = a.id
   and sem.node_id(+) = a.id

)
/
