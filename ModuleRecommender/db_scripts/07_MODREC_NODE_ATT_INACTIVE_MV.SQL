CREATE MATERIALIZED VIEW MODREC_NODE_ATT_INACTIVE_MV
  TABLESPACE TUGONLINE
  PARALLEL ( DEGREE 1 )
  BUILD IMMEDIATE
  REFRESH FORCE START WITH TO_DATE('17-11-2012 12:10 PM','DD-MM-YYYY HH12:MI PM')
    NEXT sysdate + 1
  AS
  (
select count(e.st_studium_nr)      attendance_figure
     , e.stp_knoten_nr      node_id
  from tug_new.stp_kn_stud_ergebnisse e
     , tug_new.stp_vorschrift_arten va
     , ex_students_mv student_filter
 where e.stp_vorart_nr = va.nr
   and va.kurzbezeichnung = 'ISTPOS'
   and e.flag = 'J'
   and student_filter.st_studium_nr = e.st_studium_nr
   group by e.stp_knoten_nr
)
/