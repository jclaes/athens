CREATE MATERIALIZED VIEW MODREC_MODULE_NODE_MV
  TABLESPACE TUGONLINE
  PARALLEL ( DEGREE 1 )
  BUILD IMMEDIATE
  REFRESH
    FORCE START WITH TO_DATE('17-11-2012 11:30 PM','DD-MM-YYYY HH12:MI PM')
    NEXT sysdate + 1
  AS
  (
select kn.STP_KNOTEN_NR             node_id
     , kn.STP_STP_NR                curriculum_version_id
     , kn.FA_MO_KB                  subject_type_ref_id
     , kn.FA_MO_ART_NAME            subject_type_name
     , kno.kennung                  node_shortname
     , mhb.STP_UDF_IS_PUBLIC        is_public_flag
from tug_new.pu_stp_fa_mo_knoten_v kn
   , tug_new.stp_knoten kno
   , tug_new.pu_modhb_udf_v mhb
where kno.nr = kn.stp_knoten_nr
  and mhb.STP_KNOTEN_NR = ( select tug_new.puStpModHB.getMHBTemplateNodeKey( kn.stp_knoten_nr )  from dual )
)
/