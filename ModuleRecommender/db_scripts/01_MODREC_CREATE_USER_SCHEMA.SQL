/** new Schema TUM_LEMORA for Module Recommender */
declare
  vExists   boolean;
  vExistsCO boolean;
begin
  /* CO_STP */
  vExists := false;
  vExistsCO := false;
  for users in ( select 1
                   from dba_users a
                  where a.username = 'TUM_LEMORA'
               )
  loop
    vExists := true;
  end loop;
  if not vExists then
    /* create user */
    execute immediate '
    CREATE USER TUM_LEMORA  PROFILE "DEFAULT"
      IDENTIFIED BY "TUM_LEMORA" DEFAULT TABLESPACE "TUGONLINE"
      TEMPORARY TABLESPACE "TEMP"
      ACCOUNT UNLOCK
    ';
    /* tablespace */
    execute immediate '
    GRANT UNLIMITED TABLESPACE TO "TUM_LEMORA"
    ';

    /* grants */
    execute immediate '
    GRANT "CONNECT" TO "TUM_LEMORA"
    ';
    execute immediate '
    GRANT "RESOURCE" TO "TUM_LEMORA"
    ';
  end if;

end;